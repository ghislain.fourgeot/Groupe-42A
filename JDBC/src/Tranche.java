import java.sql.*;

class Tranche {
	public char idTr;
	public int valDebut;
	public int valFin;

	public Tranche(char idTr,int valDebut,int valFin){
		this.idTr=idTr;
		this.valDebut=valDebut;
		this.valFin=valFin;
	}
}
