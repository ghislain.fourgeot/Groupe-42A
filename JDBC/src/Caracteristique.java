import java.sql.*;

class Caracteristique{
	public String idC;
	public char sexe;
	public char idTr;
	public char idCat;

	public Caracteristique(String idC,char sexe,char idTr,char idCat){
		this.idC=idC;
		this.sexe=sexe;
		this.idTr=idTr;
		this.idCat=idCat;
	}
}
