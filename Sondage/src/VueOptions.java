import javax.swing.*;

@SuppressWarnings("serial")
public class VueOptions extends JPanel{
	public VueOptions(){
		super();
		String [] nomsRadioBoutons = {"Option 1","Option 2","Option 3"};
		JRadioButton [] listeRadioBoutons;
		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		ButtonGroup choixRep = new ButtonGroup();
		listeRadioBoutons = new JRadioButton [nomsRadioBoutons.length];
		for (int i=0;i<nomsRadioBoutons.length;i++){
			listeRadioBoutons[i]=new JRadioButton(nomsRadioBoutons[i]);
			this.add(listeRadioBoutons[i]);
			choixRep.add(listeRadioBoutons[i]);
		}
		listeRadioBoutons[0].setSelected(true);
	}
}
