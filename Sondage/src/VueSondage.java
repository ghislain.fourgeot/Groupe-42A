
import java.awt.*;
import java.sql.SQLException;

import javax.swing.*;

@SuppressWarnings("serial")
public class VueSondage extends JFrame{
	VueAppel vueAppel;
	VueQuestionnaire vueQuestionnaire;
	ConnexionMySQL laConnexion;
	
	ModeleBD bd;
	public VueSondage() throws SQLException{
		super("Sondage");
		this.setSize(1050,600);
		
		
		laConnexion = new ConnexionMySQL("192.168.82.168","dbbranjauneau","branjauneau","branjauneau");
		bd = new ModeleBD(laConnexion);
		this.afficherVueAppel();
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void afficherVueAppel() throws SQLException{
		Container cont = this.getContentPane();
		cont.removeAll();
		cont.add(new VueAppel(this));
		cont.validate();
		cont.repaint();
	}
	
	public void afficherVueQuestionnaire() throws SQLException{
		Container cont = this.getContentPane();
		cont.removeAll();
		cont.add(new VueQuestionnaire(this));
		cont.validate();
		cont.repaint();
	}
	
}
