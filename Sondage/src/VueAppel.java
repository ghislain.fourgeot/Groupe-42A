import java.awt.*;
import java.sql.SQLException;
import javax.swing.*;

@SuppressWarnings("serial")
public class VueAppel extends JPanel {
	VueSondage parent;
	ModeleBD bd;

	public VueAppel(VueSondage parent) throws SQLException {
		super();
		this.parent = parent;
		this.bd = parent.bd;

		ControleurAppel ca = new ControleurAppel(this);
		this.setLayout(new BorderLayout());

		Coordonnees sondeCourant = bd.getCoordonnees();
		VueCoordonnees vCoordonnees = new VueCoordonnees(sondeCourant);

		JPanel boutons = new JPanel();
		boutons.setLayout(new FlowLayout(FlowLayout.CENTER));
		JButton ap = new JButton("Appeler");
		JLabel image = new JLabel( new ImageIcon("tel.jpg"));
		ap.setBackground(new Color(0,205,0));
		ap.setPreferredSize(new Dimension(200,30));
		ap.addActionListener(ca);
		
		boutons.add(ap);
		boutons.add(image);

		JPanel titre = new JPanel();
		titre.setLayout(new FlowLayout(FlowLayout.CENTER));
		titre.setBorder(BorderFactory.createEmptyBorder(60, 0, 0, 0));
		
		JLabel labTitre = new JLabel("Rapid'Sond");
		Font font = new Font("Arial", Font.BOLD, 50);
		labTitre.setFont(font);
		titre.add(labTitre);

		this.add(boutons, "South");
		this.add(Box.createRigidArea(new Dimension(285, 20)), "West");
		this.add(Box.createRigidArea(new Dimension(285, 20)), "East");
		this.add(titre, "North");
		this.add(vCoordonnees, "Center");
	}
}