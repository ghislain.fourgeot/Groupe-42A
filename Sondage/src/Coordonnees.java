

public class Coordonnees {
	String intituleS;
	String raisonS;
	String nomSond;
	String numTel;
	String dateNais;
	String sexeS;
	String idC;
	int numSond;
	int idPan;
	
	public Coordonnees(String intituleS, String raisonS, String nomSond,
			String numTel, String dateNais, String sexeS, String idC, int numSond, int idPan){
		this.intituleS = intituleS;
		this.raisonS=raisonS;
		this.nomSond = nomSond;
		this.numTel= numTel;
		this.dateNais=dateNais;
		this.sexeS=sexeS;
		this.idC=idC;
		this.numSond = numSond;
		this.idPan = idPan;
	}
	
	public String getIntituleS(){
		return intituleS;
	}
	public String getRaisonS(){
		return raisonS;
	}
	public String getNomSond(){
		return nomSond;
	}
	public String getNumTel(){
		return numTel;
	}
	public String getDateNais(){
		return dateNais;
	}
	public String getSexeS(){
		return sexeS;
	}
	public String getIdC(){
		return idC;
	}
	public int getNumSond(){
		return numSond;
	}
	public int getIdPan(){
		return idPan;
	}
}
