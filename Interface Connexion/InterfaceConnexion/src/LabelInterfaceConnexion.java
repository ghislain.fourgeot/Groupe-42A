
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class LabelInterfaceConnexion extends JPanel{
	LabelInterfaceConnexion(){
		setLayout(new BorderLayout());
		JLabel nomUtilisateur = new JLabel("Nom d'utilisateur : ");
		JTextField nomUtil = new JTextField(30);
		JLabel motDePasse = new JLabel("       Mot de passe : ");
		JTextField mdp = new JTextField(30);
		JButton confirmer = new JButton("Confirmer");
		JPanel gestionNom = new JPanel();
		JPanel gestionmdp = new JPanel();
		JPanel gestionConfirm = new JPanel();
		gestionNom.add(nomUtilisateur);
		gestionNom.add(nomUtil);
		gestionmdp.add(motDePasse);
		gestionmdp.add(mdp);
		gestionConfirm.add(confirmer);
		gestionNom.setLayout(new BoxLayout(gestionNom,BoxLayout.X_AXIS));
		gestionmdp.setLayout(new BoxLayout(gestionmdp,BoxLayout.X_AXIS));
		gestionConfirm.setLayout(new BoxLayout(gestionConfirm,BoxLayout.X_AXIS));
		this.add(gestionNom);
		this.add(gestionmdp);
		this.add(gestionConfirm);
		JPanel total = new JPanel();
		Component rigidArea1 = Box.createRigidArea(new Dimension(200,450));
		this.add(rigidArea1,BorderLayout.NORTH);
		Component rigidArea2 = Box.createRigidArea(new Dimension(400,200));
		this.add(rigidArea2,BorderLayout.WEST);
		Component rigidArea3 = Box.createRigidArea(new Dimension(200,450));
		this.add(rigidArea3,BorderLayout.SOUTH);
		Component rigidArea4 = Box.createRigidArea(new Dimension(400,200));
		this.add(rigidArea4,BorderLayout.EAST);
		total.add(gestionNom);
		total.add(gestionmdp);
		total.add(gestionConfirm);
		
		total.setLayout(new BoxLayout(total,BoxLayout.Y_AXIS));
		this.add(total,BorderLayout.CENTER);
		
		
		
		
		
	}
}
