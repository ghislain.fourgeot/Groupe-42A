import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class InterfaceConnexion extends JFrame{
	LabelInterfaceConnexion lb = new LabelInterfaceConnexion();
	public InterfaceConnexion(){
// construction de la fenêtre
		super("Interface de connexion Rapid'Sond");
		this.setSize(1200,1200);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container cont=this.getContentPane();
		cont.setLayout(new BorderLayout());
		cont.add(lb,"Center");
		this.setVisible(true);
		
	}
}
