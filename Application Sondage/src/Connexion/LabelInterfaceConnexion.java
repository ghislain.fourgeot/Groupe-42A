package Connexion;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;


@SuppressWarnings("serial")
public class LabelInterfaceConnexion extends JPanel{
	JButton confirmer;
	JPasswordField mdp;
	public JTextField nomUtil;
	InterfaceConnexion vue;
	
	LabelInterfaceConnexion(InterfaceConnexion vue){
		this.vue = vue;
		setLayout(new BorderLayout());
		JLabel nomUtilisateur = new JLabel("Nom d'utilisateur : ");
		this.nomUtil= new JTextField(30);
		JLabel motDePasse = new JLabel("       Mot de passe : ");
		this.mdp = new JPasswordField(30);
		this.confirmer = new JButton("Connexion");
		this.confirmer.setBackground(new Color(119, 181, 254));
		confirmer.addActionListener(new ControleurBoutonConnexion(this));
		JPanel gestionNom = new JPanel();
		JPanel gestionmdp = new JPanel();
		JPanel gestionConfirm = new JPanel();
		gestionNom.add(nomUtilisateur);
		gestionNom.add(nomUtil);
		gestionmdp.add(motDePasse);
		gestionmdp.add(mdp);
		gestionConfirm.add(confirmer);
		gestionNom.setLayout(new BoxLayout(gestionNom,BoxLayout.X_AXIS));
		gestionmdp.setLayout(new BoxLayout(gestionmdp,BoxLayout.X_AXIS));
		gestionConfirm.setLayout(new BoxLayout(gestionConfirm,BoxLayout.X_AXIS));
		this.add(gestionNom);
		this.add(gestionmdp);
		this.add(gestionConfirm);
		JPanel total = new JPanel();
		Component rigidArea1 = Box.createRigidArea(new Dimension(200,450));
		this.add(rigidArea1,BorderLayout.NORTH);
		Component rigidArea2 = Box.createRigidArea(new Dimension(400,200));
		this.add(rigidArea2,BorderLayout.WEST);
		Component rigidArea3 = Box.createRigidArea(new Dimension(200,450));
		this.add(rigidArea3,BorderLayout.SOUTH);
		Component rigidArea4 = Box.createRigidArea(new Dimension(400,200));
		this.add(rigidArea4,BorderLayout.EAST);
		total.add(gestionNom);
		total.add(gestionmdp);
		total.add(gestionConfirm);
		total.setLayout(new BoxLayout(total,BoxLayout.Y_AXIS));
		this.add(total,BorderLayout.CENTER);
		
		
		
		
		
	}
}
