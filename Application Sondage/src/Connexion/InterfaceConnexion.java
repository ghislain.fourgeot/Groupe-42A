package Connexion;

import java.awt.BorderLayout;
import java.awt.Container;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;

import Analyse.*;
import Creation.VueCreationListeQuestionnaire;
import Creation.VueCreationQuestionnaire;
import JDBC.ConnexionBDD;
import JDBC.ConnexionMySQL;
import JDBC.ModeleBD;
import Sondage.VueAppel;
import Sondage.VueQuestionnaire;

@SuppressWarnings("serial")
public class InterfaceConnexion extends JFrame{
	public LabelInterfaceConnexion lb = new LabelInterfaceConnexion(this);
	ConnexionBDD c;
	ConnexionMySQL connec;
	public RequeteAnalyse r;
	public VueCreationQuestionnaire vue;
	VueCreationListeQuestionnaire vueListe;
	VueAppel vueAppel;
	VueQuestionnaire vueQuestionnaire;
	public ModeleBD bd;
	public InterfaceConnexion() throws SQLException{
// construction de la fenêtre
		super("Interface de connexion Rapid'Sond");
		connec = new ConnexionMySQL();
		r= new RequeteAnalyse(connec,this);
		c = new ConnexionBDD(connec);
		bd = new ModeleBD(connec);
		this.setSize(1800,1000);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container cont=this.getContentPane();
		cont.setLayout(new BorderLayout());
		cont.add(lb,"Center");
		this.setVisible(true);
		
	}
	void afficherCreation() throws SQLException{
		this.getContentPane().removeAll();
		this.add(new VueCreationListeQuestionnaire(this));
		this.getContentPane().validate();
		this.getContentPane().repaint();
	}
	public void afficherAnalyse()throws SQLException{
		this.getContentPane().removeAll();
		this.add(new VueAccueilAnalyse(this));
		this.getContentPane().validate();
		this.getContentPane().repaint();
	}
	public void afficherVueQuestionnaireCreation(boolean modif) throws SQLException{
		this.getContentPane().removeAll();
		this.vue = new VueCreationQuestionnaire(this);
		this.vue.enModif = modif;
		this.getContentPane().add(vue);
		this.getContentPane().validate();
		this.getContentPane().repaint();
	}
	public void afficherVueListeQuestionnaire() throws SQLException{
		this.getContentPane().removeAll();
		this.vueListe = new VueCreationListeQuestionnaire(this);
		this.getContentPane().add(vueListe);
		this.getContentPane().validate();
		this.getContentPane().repaint();
	}
	public void afficherVueAppel() throws SQLException{
		this.getContentPane().removeAll();
		this.add(new VueAppel(this));
		this.validate();
		this.repaint();
	}
	
	public void afficherVueQuestionnaireAppel() throws SQLException{
		this.getContentPane().removeAll();
		this.add(new VueQuestionnaire(this));
		this.validate();
		this.repaint();
	}
	public void actualise(JPanel pan) {
		this.getContentPane().removeAll();
		this.getContentPane().add(pan);
		this.getContentPane().validate();
		this.getContentPane().repaint();
		
	}
}
