package Analyse;

import java.awt.*;

import javax.swing.*;

import Connexion.InterfaceConnexion;

class VueQuestion extends JPanel{
	VueTableauResult tableau;
	public VueCommentaire comm;
	public int num;
	public String intituleQ;
	InterfaceConnexion host;
	
	VueQuestion(String intituleQ, String typeQ,int num,VueTableauResult tab, VueCommentaire com, InterfaceConnexion host){
		super();
		tableau=tab;
		comm=com;
		this.host=host;
		this.num=num;
		this.intituleQ=intituleQ;
		this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		this.add(new JSeparator());
		this.add(entete(intituleQ,typeQ,num,host));
		this.add(Box.createRigidArea(new Dimension(100,25)));
		this.add(tableau);
		this.add(Box.createRigidArea(new Dimension(100,25)));
		this.add(comm);
		this.setPreferredSize(new Dimension(200,200));
	}
	
	private JPanel entete (String intituleQ, String typeQ,int num,InterfaceConnexion host2){
		JPanel res = new JPanel();
		JPanel info = new JPanel();
		info.setLayout(new BorderLayout());
		info.add(new JLabel("Question"+num+" : "+intituleQ),"North");
		info.add(new JLabel("Type question :"+typeQ),"South");
		res.setLayout(new BoxLayout(res,BoxLayout.LINE_AXIS));
		res.add(info);
		res.add(Box.createRigidArea(new Dimension(100,50)));
		JButton Aperçu = new JButton("Aperçu");
		Aperçu.addActionListener(new ControlleurAperçu(this,host2));
		res.add(Aperçu);
		res.add(Box.createRigidArea(new Dimension(100,50)));
		res.setPreferredSize(new Dimension(200,75));
		return res;
	}

}