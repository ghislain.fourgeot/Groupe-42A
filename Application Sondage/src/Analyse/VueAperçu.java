package Analyse;


import java.awt.Color;
import java.awt.Dimension;

import javax.swing.*;

import org.jfree.chart.*;
import org.jfree.data.general.Dataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;


class VueAperçu extends JPanel{
	String intituleQ;
	int numQ;
	VueQuestion parent;
	JButton retour;
	
	VueAperçu(String intituleQ, int numQ,JPanel vue,VueQuestion parent){
		//vue.setPreferredSize(new Dimension(250,300));
		vue.setMaximumSize(new Dimension(1200,450));
		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		JLabel nom=new JLabel("Question"+numQ+" : "+intituleQ);
		this.intituleQ=intituleQ;
		this.numQ=numQ;
		this.add(nom);
		this.add(vue);
		this.parent=parent;
		JLabel titre = new JLabel("Commentaire :");
		JLabel commentaire = new JLabel(parent.comm.com.getText());
		this.add(titre);
		this.add(commentaire);
		retour = new JButton("Retour");
		retour.setName("a effacer");
		retour.addActionListener(new ControlleurAperçu(parent,parent.host));
		this.add(retour);
		this.setMaximumSize(new Dimension(500,300));
	}
}