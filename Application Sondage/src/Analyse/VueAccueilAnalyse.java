package Analyse;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.*;

import Connexion.InterfaceConnexion;

public class VueAccueilAnalyse extends JPanel{
	public RequeteAnalyse r;
	public VuedonneeSondage vue;
	public VueAccueilAnalyse(InterfaceConnexion interfaceConnexion)throws SQLException{
		this.setLayout(new BorderLayout ());
		this.r = interfaceConnexion.r;
		VueListeSondage liste = new VueListeSondage(r.createAccueilAnalyse(),interfaceConnexion);
		this.add(AjoutScroll(liste));
		
	}
	private JScrollPane AjoutScroll(VueListeSondage vue){
		JScrollPane res = new JScrollPane(vue);
		res.setPreferredSize(new Dimension(100,225));
		return res;
		
	}
}