package Analyse;

import java.awt.*;
import java.util.ArrayList;

import javax.swing.*;

import Connexion.InterfaceConnexion;

class VueListeSondage extends JPanel{
	
	VueListeSondage(ArrayList<VuedonneeSondage> liste,InterfaceConnexion interfaceConnexion){
		this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		for (int i=0;i<liste.size();i++){
			this.add(creation(liste.get(i),interfaceConnexion));
		}
		this.add(Box.createRigidArea(new Dimension(300,225)));
	}

	public JPanel creation(VuedonneeSondage vue,InterfaceConnexion interfaceConnexion){
		JPanel res = new JPanel();
		res.setLayout(new BoxLayout(res,BoxLayout.LINE_AXIS));
		JButton c = new JButton("Consulter");
		c.setSize(20,10);
		c.addActionListener(new ControlleurConsulter(vue,interfaceConnexion));
		JPanel butt = new JPanel();
		butt.setLayout(new BorderLayout());
		butt.add(Box.createRigidArea(new Dimension(10,25)),"North");
		butt.add(Box.createRigidArea(new Dimension(10,25)),"South");
		butt.add(Box.createRigidArea(new Dimension(25,10)),"East");
		butt.add(Box.createRigidArea(new Dimension(25,10)),"West");
		butt.add(c,"Center");

		vue.add(butt,"East");
		res.add(vue, "North");
		res.add(new JLabel(" "),"South");
		res.setPreferredSize(new Dimension(200,75));
		return res;
	}
}
