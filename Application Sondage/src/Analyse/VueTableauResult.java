package Analyse;

import java.awt.*;

import javax.swing.*;

class VueTableauResult extends JPanel{
	
	VueTableauResult(Object[][] donne ,String [] intitule){
		JTable tableau = new JTable(donne,intitule);
		this.setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
		this.add(Box.createRigidArea(new Dimension(50,75)));
		JPanel tab = new JPanel();
		tab.setLayout(new BorderLayout());
		tab.add(tableau.getTableHeader(),"North");
		tab.add(tableau,"South");
		JScrollPane tabl = new JScrollPane(tab);
		tabl.getVerticalScrollBar().setUnitIncrement(16);
		this.add(tabl);
		this.add(Box.createRigidArea(new Dimension(50,75)));
	}
}