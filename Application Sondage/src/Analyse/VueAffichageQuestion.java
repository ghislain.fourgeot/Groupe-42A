package Analyse;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.*;

import Connexion.InterfaceConnexion;

class VueAffichageQuestion extends JPanel{
	
	RequeteAnalyse r;
	InterfaceConnexion host ;
	VuedonneeSondage vue ;
	ArrayList<VueQuestion> liste;
	VueAffichageQuestion(VuedonneeSondage cli,InterfaceConnexion host2) throws SQLException{
		this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		this.host=host2;
		this.add(cli);
		this.vue=cli;
		this.setName("AffichageQuestion");
		this.r=host2.r;
		this.modifcli(host2);
		liste = r.createAffichageQuestion(cli.intitule, cli.cli);
		this.add(AjoutScroll(new VueListeQuestion(liste)));
		
		
	}
	private JScrollPane AjoutScroll(JPanel vue){
		JScrollPane res = new JScrollPane(vue);
		res.setPreferredSize(new Dimension(400,480));
		res.getVerticalScrollBar().setUnitIncrement(16);
		return res;
	}
	private void modifcli(InterfaceConnexion host2){
		JPanel PDF =new JPanel();
		PDF.setLayout(new BorderLayout());
		PDF.add(Box.createRigidArea(new Dimension(10,25)),"North");
		PDF.add(Box.createRigidArea(new Dimension(10,25)),"South");
		PDF.add(Box.createRigidArea(new Dimension(25,10)),"East");
		PDF.add(Box.createRigidArea(new Dimension(25,10)),"West");
		JPanel boutons = new JPanel();
		JButton retour = new JButton("Retour");
		retour.addActionListener(new ControlleurConsulter(vue,host2));
		retour.setBackground(Color.red);
		JButton pdf = new JButton("Aperçu PDF");
		pdf.addActionListener(new ControlleurConsulter(vue,host2));
		boutons.add(retour);
		boutons.add(pdf);
		PDF.add(boutons,"Center");
		pdf.setBackground(Color.green);
		vue.add(PDF,"East");
	
	}
}