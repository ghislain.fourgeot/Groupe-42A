package Analyse;

import java.awt.*;

import javax.swing.*;

class VueCommentaire extends JPanel{
	JComboBox type;
	JTextArea com;
	VueCommentaire(){
		super();
		String [] diag = {"","Circulaire","Barre verticale","Barre horizontale","Courbe","Tableau"};
		JComboBox<String> diagramme = new JComboBox <String>(diag);
		type=diagramme;
		diagramme.setPreferredSize(new Dimension(100,12));
		JPanel diagr = new JPanel();
		diagr.setLayout(new BorderLayout());
		diagr.add(Box.createRigidArea(new Dimension(150,31)),"North");
		diagr.add(Box.createRigidArea(new Dimension(50,1)),"West");
		diagr.add(type,"Center");
		diagr.add(Box.createRigidArea(new Dimension(150,32)),"South");
		JPanel texte = new JPanel();
		texte.setLayout(new BorderLayout());
		texte.add(new JLabel("Commentaire:"),"North");
		com = new JTextArea(100,75);
		texte.add(com,"Center");
		texte.add(Box.createRigidArea(new Dimension(10,10)),"South");
		this.setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
		this.add(diagr);
		this.add(Box.createRigidArea(new Dimension(200,75)));
		this.add(texte);
		
		
	}
}