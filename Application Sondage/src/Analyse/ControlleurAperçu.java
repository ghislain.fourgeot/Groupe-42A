package Analyse;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.jfree.data.general.DefaultPieDataset;

import Connexion.InterfaceConnexion;

class ControlleurAperçu implements ActionListener{
	VueQuestion vue;
	InterfaceConnexion host;
	ControlleurAperçu(VueQuestion vue, InterfaceConnexion host2){
		this.vue=vue;
		this.host=host2;
	}

	public void actionPerformed(ActionEvent arg0) {
		switch(((JButton) arg0.getSource()).getText())
		{
		case "Aperçu":
			switch(vue.comm.type.getSelectedIndex()){
			case 0:
				JOptionPane.showMessageDialog(null,"Aucun type de diagramme n'as été selectionné");
				break;
			case 1:
				try {
					host.actualise(new VueAperçu(vue.intituleQ,vue.num,host.r.creationAperçuQPie(vue.intituleQ, vue.num),vue));
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println(e.getMessage());
				}
				break;
			case 2:
				try {
					host.actualise(new VueAperçu(vue.intituleQ,vue.num,host.r.creationAperçuQBar(vue.intituleQ, vue.num,true),vue));
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println(e.getMessage());
				}
				break;
			case 3:
				try {
					host.actualise(new VueAperçu(vue.intituleQ,vue.num,host.r.creationAperçuQBar(vue.intituleQ, vue.num,false),vue));
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println(e.getMessage());
				}
				break;
			case 4:
				try {
					host.actualise(new VueAperçu(vue.intituleQ,vue.num,host.r.creationAperçuQLine(vue.intituleQ, vue.num),vue));
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println(e.getMessage());
				}
				break;
			case 5:
				try {
					host.actualise(new VueAperçu(vue.intituleQ,vue.num,host.r.creationAperçuQTableau(vue.intituleQ, vue.num),vue));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			}
			break;
		case "Retour" :
			host.actualise((JPanel) SwingUtilities.getAncestorNamed("AffichageQuestion", vue));
			break;
			
			
		}
		
	}
	
}