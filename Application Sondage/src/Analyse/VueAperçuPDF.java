package Analyse;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.*;

class VueAperçuPDF extends JPanel{
	VuedonneeSondage cli;
	ArrayList<VueAperçu> questions;
	VueAperçuPDF(VuedonneeSondage cli,ArrayList<VueAperçu> questions, VueAffichageQuestion parent){
		this.questions=questions;
		JPanel bouttons = new JPanel();
		bouttons.setLayout(new BoxLayout(bouttons,BoxLayout.LINE_AXIS));
		JButton retour = new JButton("Retour");
		retour.addActionListener(new ControlleurAperçu(parent.liste.get(0),parent.host));
		bouttons.add(retour);
		JButton envoyer = new JButton("Envoyer");
		envoyer.addActionListener(new ControlleurPDF(this));
		bouttons.add(envoyer);
		
		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		this.cli = new VuedonneeSondage(cli.intitule,cli.cli,cli.res);
		this.add(bouttons);
		this.add(this.cli);
		JPanel Qs = new JPanel();
		Qs.setLayout(new GridLayout(questions.size(),1));
		Qs.setPreferredSize(new Dimension(400,2000));
		for (VueAperçu aperçu :questions){
			aperçu.remove(aperçu.retour);
			Qs.add(aperçu);
		}
		JScrollPane res = new JScrollPane(Qs);
		res.setPreferredSize(new Dimension(400,500));
		res.getVerticalScrollBar().setUnitIncrement(16);
		this.add(res);
		
	}
}