package Analyse;

import javax.imageio.ImageIO;
import javax.swing.*;

import com.sun.prism.paint.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;


import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDFontFactory;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;


public class ControlleurPDF implements ActionListener{
	VueAperçuPDF vue;
	public ControlleurPDF(VueAperçuPDF vue){
		this.vue=vue;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		PDDocument pdf = new PDDocument();
		PDPage page = new PDPage();
		pdf.addPage(page);
		BufferedImage img = new BufferedImage(vue.cli.getWidth(),vue.cli.getHeight(), BufferedImage.TYPE_INT_RGB);
		img.setRGB(50,50,Color.RED.getIntArgbPre());
	    vue.cli.paint(img.getGraphics());
	    try {
	        ImageIO.write(img, "png", new File("../Screen.png"));
	    } catch (Exception e) {
	        System.out.println("panel not saved" + e.getMessage());
	    }
	    PDPageContentStream contentStream;
		try {
			contentStream = new PDPageContentStream(pdf, page);
			PDImageXObject pdImage = PDImageXObject.createFromFile("../Screen.png", pdf);
			contentStream.drawImage(pdImage, 100, 200);
			contentStream.close();
			for (VueAperçu aperçu :vue.questions){
				page = new PDPage();
				pdf.addPage(page);
				contentStream = new PDPageContentStream(pdf, page);
				img = new BufferedImage(aperçu.getWidth()-150,aperçu.getHeight()-150, BufferedImage.TYPE_INT_RGB);
				img.setRGB(50,50,Color.RED.getIntArgbPre());
				aperçu.paint(img.getGraphics());
				try {
					ImageIO.write(img, "png", new File("../Screen.png"));
					} 
				catch (Exception e) {
					System.out.println("panel not saved" + e.getMessage());
					}
				pdImage = PDImageXObject.createFromFile("../Screen.png", pdf);
				contentStream.drawImage(pdImage, 10, 10);
				contentStream.close();
			}
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			pdf.save("../test.pdf");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		JOptionPane.showMessageDialog(null,"Le pdf a bien été généré");
		try {
			vue.questions.get(0).parent.host.actualise(new VueAccueilAnalyse(vue.questions.get(0).parent.host));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}