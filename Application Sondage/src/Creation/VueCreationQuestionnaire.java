package Creation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import Connexion.InterfaceConnexion;
import JDBC.ConnexionMySQL;
import JDBC.InsertionBDDNouveauQuestionnaire;
import JDBC.SupprimerBDDQuestionnaire;

//Classe de création de l'interface d'un questionnaire
public class VueCreationQuestionnaire extends JPanel {
	
	//Attributs
	public ArrayList<VueCreaionQuest> listP;
	VueListeQuestion list;
	JScrollPane scroll;
	public VueInformationsCreation vueInfo;
	public char etat;
	public int idQuestionnaire;
	public InterfaceConnexion crea;
	InsertionBDDNouveauQuestionnaire r;
	SupprimerBDDQuestionnaire s;
	ConnexionMySQL connec;
	public boolean enModif;
	//Constructeur
	public VueCreationQuestionnaire(InterfaceConnexion crea) throws SQLException {
		super();
		
		//Variables
		this.connec = new ConnexionMySQL();
		r = new InsertionBDDNouveauQuestionnaire(connec);
		s = new SupprimerBDDQuestionnaire(connec);
		this.crea = crea;
		this.listP = new ArrayList<VueCreaionQuest>();
		this.vueInfo = new VueInformationsCreation();
		this.setLayout(new BorderLayout());
		this.add(new VueEnrSupp(this),BorderLayout.NORTH);
		this.add(vueInfo,BorderLayout.WEST);
		this.add(new VueAjoutQuestion(this),BorderLayout.EAST);
		this.list = new VueListeQuestion(listP);
		this.scroll = AjoutScroll(list);
		this.add(scroll);
	}
	
	//Ajout d'une question
	public void ajoutQuestion(){
		listP.add(0,new VueCreaionQuest(this));
	}
	
	//Suppression d'une question
	public void supprimerQuestion(VueCreaionQuest question){
		listP.remove(question);
	}
	
	//Rafraichissement de l'interface
	public void majInterface(){
		this.removeAll();
		this.add(new VueEnrSupp(this),BorderLayout.NORTH);
		this.add(vueInfo,BorderLayout.WEST);
		this.add(new VueAjoutQuestion(this),BorderLayout.EAST);
		list = new VueListeQuestion(listP);
		scroll = AjoutScroll(list);
		this.add(scroll);
		this.repaint();
		this.validate();
		this.revalidate();
	}
	
	//Création de la liste déroulante des questions
	private JScrollPane AjoutScroll(VueListeQuestion vue){
		JScrollPane res = new JScrollPane(vue);
		if(listP.size() == 0){
			res.setBorder(BorderFactory.createLineBorder(Color.black,8));
		}
		res.getVerticalScrollBar().setUnitIncrement(16);
		return res;
		
	}
}
