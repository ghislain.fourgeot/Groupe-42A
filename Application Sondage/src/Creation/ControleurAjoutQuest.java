package Creation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


//Controleur du bouton "Ajouter question"
public class ControleurAjoutQuest implements ActionListener{
	
	//Attributs
	VueCreationQuestionnaire vueCrea;
	
	//Constructeur
	ControleurAjoutQuest(VueCreationQuestionnaire vue){
		
		//Variables
		this.vueCrea = vue;
	}
	
	//Ajout d'une question lors de l'appui sur le bouton
	public void actionPerformed(ActionEvent arg0) {
		vueCrea.ajoutQuestion();
		vueCrea.majInterface();
	}

}
