package Creation;

import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

//Panel de la liste des réponses
public class VueListeReponse extends JPanel {
	
	//Attributs
	ArrayList<VueReponse> list;
	
	//Constructeur
	VueListeReponse(ArrayList<VueReponse> list){
		
		//Varaibles
		this.list = list;
		
		this.setLayout(new GridLayout(list.size(),1));
		for(VueReponse reponse : list){
			this.add(reponse);
		}
		this.setSize(100,200);
	}
}
