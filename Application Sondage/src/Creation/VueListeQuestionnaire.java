package Creation;

import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

//Panel de la liste des questions
public class VueListeQuestionnaire extends JPanel {
	
	//Atributs
	ArrayList<VueQuestionnairePetit> list;
	
	//Constructeur
	VueListeQuestionnaire(ArrayList<VueQuestionnairePetit> list){
		
		//Variables
		this.list = list;
		
		this.setLayout(new GridLayout(list.size(),1));
		for(VueQuestionnairePetit questionnaire : list){
			this.add(questionnaire);
		}
		this.setSize(400,200);
	}
}
