package Creation;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;

//Panel du bouton "Ajouter nouvelle réponse"
public class VueAjoutReponseBouton extends JPanel {
	
	//Attributs
	VueCreationQuestionnaire vueQuest;
	VueCreaionQuest vue;
	JButton ajouterQuestion;
	
	//Constructeur
	VueAjoutReponseBouton(VueCreationQuestionnaire vueQuest, VueCreaionQuest vue){
		
		//Variables
		this.vueQuest = vueQuest;
		this.vue = vue;
		this.setLayout(new BorderLayout());
		this.ajouterQuestion = new JButton("Ajouter nouvelle réponse");
		this.add(Box.createRigidArea(new Dimension(62, 0)),BorderLayout.WEST);
		this.add(Box.createRigidArea(new Dimension(57, 0)),BorderLayout.EAST);
		this.add(Box.createRigidArea(new Dimension(0, 50)),BorderLayout.NORTH);
		this.add(Box.createRigidArea(new Dimension(0, 50)),BorderLayout.SOUTH);
		ajouterQuestion.addActionListener(new ControleurAjoutReponse(vue,vueQuest));
		this.add(ajouterQuestion);
	}
	
}
