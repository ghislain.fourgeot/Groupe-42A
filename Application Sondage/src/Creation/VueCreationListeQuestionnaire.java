package Creation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import Connexion.InterfaceConnexion;
import JDBC.ConnexionMySQL;
import JDBC.RecuperationBDDQuestionnaire;


public class VueCreationListeQuestionnaire extends JPanel {
	public ArrayList<VueQuestionnairePetit> listQ;
	VueListeQuestionnaire vueListQ;
	JScrollPane scroll;
	public InterfaceConnexion crea;
	VueBoutonNouveau nouveau;
	ConnexionMySQL connec;
	public VueCreationListeQuestionnaire(InterfaceConnexion crea)throws SQLException{
		listQ = new ArrayList<VueQuestionnairePetit>();
		try{
			connec = new ConnexionMySQL();
			RecuperationBDDQuestionnaire r = new RecuperationBDDQuestionnaire(connec);
			r.ajoutListe(this);
			connec.mysql.close();
		}
		catch(SQLException e){
			System.out.println("Erreur dans la base de donnée");
		}
		this.crea = crea;
		this.nouveau = new VueBoutonNouveau(this);
		vueListQ = new VueListeQuestionnaire(listQ);
		scroll = AjoutScroll(vueListQ);
		this.setLayout(new BorderLayout());
		this.add(nouveau,BorderLayout.NORTH);
		this.add(scroll);
		
	}
	private JScrollPane AjoutScroll(VueListeQuestionnaire vue){
		JScrollPane res = new JScrollPane(vue);
		res.getVerticalScrollBar().setUnitIncrement(16);
		res.setSize(new Dimension(600,400));
		return res;
	}
	public void majInterface(){
		this.removeAll();
		listQ.clear();
		try{
			connec.mysql.close();
			RecuperationBDDQuestionnaire r = new RecuperationBDDQuestionnaire(new ConnexionMySQL());
			r.ajoutListe(this);
		}
		catch(SQLException e){
			System.out.println("Erreur dans la base de donnée");
		}
		this.crea = crea;
		this.nouveau = new VueBoutonNouveau(this);
		vueListQ = new VueListeQuestionnaire(listQ);
		scroll = AjoutScroll(vueListQ);
		this.setLayout(new BorderLayout());
		this.add(nouveau,BorderLayout.NORTH);
		this.add(scroll);
		this.repaint();
		this.validate();
		this.revalidate();
	}
}
