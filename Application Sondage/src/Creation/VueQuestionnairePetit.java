package Creation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import JDBC.ConnexionMySQL;
import JDBC.ModificationBDDQuestionnaire;
import JDBC.SupprimerBDDQuestionnaire;


public class VueQuestionnairePetit extends JPanel {
	public VueTitreDescPetitQuest vueHaut;
	public VueBoutonPetitQuest vueBas;
	public int idQuestionnaire;
	SupprimerBDDQuestionnaire r;
	ModificationBDDQuestionnaire m;
	public VueCreationListeQuestionnaire vue;
	ConnexionMySQL connec;
	public VueQuestionnairePetit(VueCreationListeQuestionnaire vue){
		connec =new ConnexionMySQL();
		try {
			r = new SupprimerBDDQuestionnaire(connec);
			m = new ModificationBDDQuestionnaire(connec);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.vue = vue;
		this.vueHaut = new VueTitreDescPetitQuest();
		this.vueBas = new VueBoutonPetitQuest(this);
		this.setLayout(new BorderLayout());
		this.add(vueHaut,BorderLayout.NORTH);
		this.add(vueBas,BorderLayout.CENTER);
		this.setPreferredSize(new Dimension(800,100));
		this.setBorder(BorderFactory.createLineBorder(Color.black,1));
	}
}
