package Creation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import Connexion.InterfaceConnexion;


public class ControleurNouveauQuestionnaire implements ActionListener{
	InterfaceConnexion crea;
	ControleurNouveauQuestionnaire(InterfaceConnexion crea){
		this.crea = crea;
	}
	public void actionPerformed(ActionEvent arg0) {
		try {
			crea.afficherVueQuestionnaireCreation(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
