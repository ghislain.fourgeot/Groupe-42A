package Creation;

import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

//Controleur de la JComboBox du type de la question
class ControleurTypeQuestionChange implements ItemListener{
	
	//Attributs
	VueCreaionQuest vue;
	
	//Constructeur
	ControleurTypeQuestionChange(VueCreaionQuest vue){
		
		//Variables
		this.vue = vue;
	}
	
	//Suppression des réponses, et mise à jour de l'interface lors de l'appui sur le bouton
    public void itemStateChanged(ItemEvent arg0) {
		if (arg0.getStateChange() == ItemEvent.SELECTED) {
			vue.listR.clear();
			vue.majInterface();
			if(vue.getTypeQuestion() == 4 || vue.getTypeQuestion() == 2){
				vue.ajouteReponse();
				vue.ajouterR.ajouterQuestion.setEnabled(false);
			}
			else{
				vue.ajouterR.ajouterQuestion.setEnabled(true);
			}
			vue.majInterface();
		}
	}       
}