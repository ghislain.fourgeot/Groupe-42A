package Creation;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JPanel;

//Panel des boutons "enregistrer" et "supprimer" le questionnaire
public class VueEnrSupp extends JPanel{
	VueCreationQuestionnaire vue;
	//Constructeur
	VueEnrSupp(VueCreationQuestionnaire vue){
		super();
		this.vue = vue;
		JButton enregistrer = new JButton("Enregistrer le questionnaire");
		enregistrer.addActionListener(new ControleurEnregistrer(vue));
		enregistrer.setBackground(new Color(50,165,15));
		JButton supprimer = new JButton("Quitter le questionnaire");
		supprimer.addActionListener(new ControleurSuppQuestionnaire(vue));
		supprimer.setBackground(new Color(253,31,31));
		this.add(enregistrer);
		this.add(supprimer);
	}
}
