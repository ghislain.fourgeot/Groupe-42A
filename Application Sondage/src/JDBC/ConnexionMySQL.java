package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnexionMySQL {
	
	//Attributs
	public Connection mysql=null;
    boolean connecte=false;
    String nomServeur = "192.168.82.168";
    String nomBase = "dbstede";
    String nomLogin = "stede";
    String motDePasse = "stede";
    
    //Constructeur
	public ConnexionMySQL() {
	    try{
				Class.forName("com.mysql.jdbc.Driver");
			}
			catch(ClassNotFoundException e){
				System.out.println("Driver MySQL non trouvé");
				mysql = null;
				return;
			}
			try{
				mysql = DriverManager.getConnection("jdbc:mysql://"+nomServeur+":3306/"+nomBase,nomLogin,motDePasse);
				connecte = true;
			}
			catch(SQLException e){
				System.out.println("Echec de connexion");
				System.out.println(e.getMessage());
				mysql = null;
				return;
			}
	}
	public boolean getConnecte(){
		return connecte;
	}

}
