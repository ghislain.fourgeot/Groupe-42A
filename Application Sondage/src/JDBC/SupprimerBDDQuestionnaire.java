package JDBC;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import Creation.VueCreationQuestionnaire;
import Creation.VueQuestionnairePetit;

//Classe d'ajout d'un nouveau questionnaire dans la base de donnée
public class SupprimerBDDQuestionnaire {
	
	  //Attributs
	  ConnexionMySQL laConnexion=null;
	  Statement st;
	  
	  //Constructeur
	  public SupprimerBDDQuestionnaire(ConnexionMySQL c)throws SQLException{
		  
		  //Variables
		  laConnexion=c;
		  st = laConnexion.mysql.createStatement();
	  }
	  
	  //Ajout du questionnaire à la base de donnée
	  public void supprimerQuestionnaire(VueQuestionnairePetit questionnaire){
		  try{
			  PreparedStatement ps = laConnexion.mysql.prepareStatement("delete from VALPOSSIBLE where idQ = ?");
		      ps.setInt(1,questionnaire.idQuestionnaire);
		      ps.executeUpdate();
		      PreparedStatement ps2 = laConnexion.mysql.prepareStatement("delete from QUESTION where idQ = ?");
		      ps2.setInt(1,questionnaire.idQuestionnaire);
		      ps2.executeUpdate();
		      PreparedStatement ps3 = laConnexion.mysql.prepareStatement("delete from QUESTIONNAIRE where idQ = ?");
		      ps3.setInt(1,questionnaire.idQuestionnaire);
		      ps3.executeUpdate();
		      st.close();
		      }
		  catch(SQLException e){
		      System.out.println("Pas de table QUESTIONNAIRE");
		      e.printStackTrace();
		  }
	  }
	  public void exploitQuestionnaire(VueQuestionnairePetit vue){
		  try {
			PreparedStatement ps = laConnexion.mysql.prepareStatement("update QUESTIONNAIRE set Etat='S' where idQ= ?");
			ps.setInt(1,vue.idQuestionnaire);
			ps.executeUpdate();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	  }
	  public void supprimerQuestionnaireModif(VueCreationQuestionnaire questionnaire){
		  try{
			  PreparedStatement ps = laConnexion.mysql.prepareStatement("delete from VALPOSSIBLE where idQ = ?");
		      ps.setInt(1,questionnaire.idQuestionnaire);
		      ps.executeUpdate();
		      PreparedStatement ps2 = laConnexion.mysql.prepareStatement("delete from QUESTION where idQ = ?");
		      ps2.setInt(1,questionnaire.idQuestionnaire);
		      ps2.executeUpdate();
		      PreparedStatement ps3 = laConnexion.mysql.prepareStatement("delete from QUESTIONNAIRE where idQ = ?");
		      ps3.setInt(1,questionnaire.idQuestionnaire);
		      ps3.executeUpdate();
		      st.close();
		      }
		  catch(SQLException e){
		      System.out.println("Pas de table QUESTIONNAIRE");
		      e.printStackTrace();
		  }
	  }
}