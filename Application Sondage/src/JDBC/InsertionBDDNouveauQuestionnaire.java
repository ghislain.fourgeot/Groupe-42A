package JDBC;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Creation.VueCreationQuestionnaire;

//Classe d'ajout d'un nouveau questionnaire dans la base de donnée
public class InsertionBDDNouveauQuestionnaire {
	
	  //Attributs
	  ConnexionMySQL laConnexion=null;
	  Statement st;
	  Statement st2;
	  Statement st3;
	  
	  //Constructeur
	  public InsertionBDDNouveauQuestionnaire(ConnexionMySQL c)throws SQLException{
		  
		  //Variables
		  laConnexion=c;
		  st = laConnexion.mysql.createStatement();
		  st2 = laConnexion.mysql.createStatement();
		  st3 = laConnexion.mysql.createStatement();
	  }
	  //Ajout du questionnaire à la base de donnée
	  public void nouveauQuestionnaire(VueCreationQuestionnaire questionnaire){
		  int idQuestionnaire = 0;
		  try{
		        ResultSet rs = st.executeQuery("select max(idQ) from QUESTIONNAIRE");
		        rs.next();
		        idQuestionnaire = rs.getInt(1) + 1;
		        questionnaire.idQuestionnaire = idQuestionnaire;
		      }
		      catch(SQLException e){
		        System.out.println("Pas de table QUESTIONNAIRE");
		        e.printStackTrace();
		      }
	       try{
	    	  ResultSet ah = st3.executeQuery("select * from UTILISATEUR where login = '"+questionnaire.crea.lb.nomUtil.getText()+"'");
			  ah.next();
	    	  ResultSet ps2 = st2.executeQuery("select * from PANEL where nomPan = '"+questionnaire.vueInfo.panel.getSelectedItem()+"'");
	    	  ps2.next();
		      PreparedStatement ps = laConnexion.mysql.prepareStatement("insert into QUESTIONNAIRE values(?,?,?,?,?,?)");
	          ps.setInt(1,idQuestionnaire);
	          ps.setString(2,questionnaire.vueInfo.titreText.getText());
	          ps.setString(3,String.valueOf(questionnaire.etat));
	          ps.setInt(4, 15678);
	          ps.setInt(5, ah.getInt("idU"));
	          ps.setInt(6, ps2.getInt("idPan"));
	          ps.executeUpdate();
	          ah.close();
	          ps.close();
	        }
	        catch(SQLException e){
	          System.out.println("Pas de table QUESTIONNAIRE2");
	    	  System.out.println("select * from PANEL where nomPan = '"+questionnaire.vueInfo.panel.getSelectedItem()+"'");
	          e.printStackTrace();
	        }
		}
	  
	  //Ajout des questions à la base de donnée
	  public void nouvelleQuestion(VueCreationQuestionnaire questionnaire){
		  try{
			  for(int i = 0; i< questionnaire.listP.size();i++){
				  PreparedStatement ps2 = laConnexion.mysql.prepareStatement("insert into QUESTION values(?,?,?,?,?)");
				  ps2.setInt(1,questionnaire.idQuestionnaire);
				  ps2.setInt(2,i+1);
				  ps2.setString(3,questionnaire.listP.get(i).vueIntitule.titreQ.getText());
				  if((questionnaire.listP.get(i).getTypeQuestion() == 0)||(questionnaire.listP.get(i).getTypeQuestion() == 3)){
					  ps2.setInt(4,questionnaire.listP.get(i).listR.size());
				  }
				  else if(questionnaire.listP.get(i).getTypeQuestion() == 0){
					  ps2.setInt(4,10);
				  }
				  else{
					  ps2.setObject(4, null);
				  }
				  switch(questionnaire.listP.get(i).getTypeQuestion()){
				  	case 0:
				  		ps2.setString(5,"m");
				  		break;
				  	case 1:
				  		ps2.setString(5,"u");
				  		break;
				  	case 2: 
				  		ps2.setString(5,"l");
				  		break;
				  	case 3:
				  		ps2.setString(5,"c");
				  		break;
				  	case 4:
				  		ps2.setString(5,"n");
				  		break;
				  }
				  ps2.executeUpdate();
			  }
	        }
	        catch(SQLException e){
	          System.out.println("Pas de table QUESTION");
	          e.printStackTrace();
	        }
	  }
	  
	  //Ajout des possibilités de réponse à la base de donnée
	  public void nouvelleValPoss(VueCreationQuestionnaire questionnaire){
		  try{
			  for(int i = 0; i< questionnaire.listP.size();i++){
				  if((questionnaire.listP.get(i).getTypeQuestion() == 0)||(questionnaire.listP.get(i).getTypeQuestion() == 1)||(questionnaire.listP.get(i).getTypeQuestion() == 3)){
					  for(int j = 0; j< questionnaire.listP.get(i).listR.size();j++){
						  PreparedStatement ps = laConnexion.mysql.prepareStatement("insert into VALPOSSIBLE values(?,?,?,?)");
					  	  ps.setInt(1,questionnaire.idQuestionnaire);
					  	  ps.setInt(2,i+1);
					  	  ps.setInt(3,j+1);
					  	  ps.setString(4,questionnaire.listP.get(i).listR.get(j).resultat.getText());
					  	  ps.executeUpdate();
					  }
				  }
			  }
		  }
		  catch(SQLException e){
	          System.out.println("Pas de table QUESTIONNAIRE");
	      }
		  
	  }
}