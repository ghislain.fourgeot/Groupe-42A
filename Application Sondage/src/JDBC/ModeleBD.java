package JDBC;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Random;

import Sondage.Coordonnees;
import Sondage.Question;
import Sondage.Sonde;

public class ModeleBD {
	ConnexionMySQL laConnection;
	public int num;
	Random random;
	ArrayList<Coordonnees> listeCoord;

	public ModeleBD(ConnexionMySQL laConnection) {
		super();
		this.laConnection = laConnection;
		this.random = new Random();
		this.listeCoord = new ArrayList<Coordonnees>();
		this.num = random.nextInt(1000);
	}

	public ArrayList<Question> listeDesQuestions(int numQuestionnaire)
			throws SQLException {
		ArrayList<Question> res = new ArrayList<Question>();
		Statement ps = laConnection.mysql.createStatement();
		ResultSet rs = ps
				.executeQuery("select numQ, texteQ, MaxVal, idT from QUESTION where idQ="
						+ numQuestionnaire + " order by numQ");
		while (rs.next()) {
			res.add(new Question(rs.getInt(1), rs.getString(2), rs.getInt(3),
					rs.getString(4).charAt(0)));
		}
		rs.close();
		return res;
	}

	ArrayList<Sonde> listeInterroger(int idPanel, int idQuestionnaire)
			throws SQLException {
		PreparedStatement ps = laConnection.mysql
				.prepareStatement("select numSond,idC from SONDE natural join CONSTITUER "
						+ "where idPan= ? and numSond not in "
						+ "(select numSond from INTERROGER where idQ=?)");
		ps.setInt(1, idPanel);
		ps.setInt(2, idQuestionnaire);
		ResultSet rs = ps.executeQuery();
		ArrayList<Sonde> res = new ArrayList<Sonde>();
		while (rs.next()) {
			res.add(new Sonde(rs.getInt(1), rs.getString(2)));
		}
		rs.close();
		return res;
	}

	/*
	 DefaultComboBoxModel<Questionnaire> listeDesQuestionnairesPrets()throws SQLException {
		Statement ps = laConnection.mysql.createStatement();
		ResultSet rs = ps
				.executeQuery("select idQ, Titre, idPan, count(*) taillePan from QUESTIONNAIRE natural join PANEL natural join CONSTITUER "
						+ "where Etat='S' and idQ in (select idQ from QUESTION) and (numSond,idQ) not in (select numSond,idQ from INTERROGER) group by  idQ, Titre, idPan order by Titre");
		DefaultComboBoxModel<Questionnaire> res = new DefaultComboBoxModel<Questionnaire>();
		while (rs.next()) {
			res.addElement(new Questionnaire(rs.getInt(1), rs.getString(2),
					rs.getInt(3), rs.getInt(4)));
		}
		rs.close();
		return res;
	}
	
	ArrayList<Integer> listeSondeurs() throws SQLException {
		PreparedStatement ps = laConnection.mysql
				.prepareStatement("select idU from UTILISATEUR natural join ROLEUTIL "
						+ "where nomR= 'Sondeur'");
		ResultSet rs = ps.executeQuery();
		ArrayList<Integer> res = new ArrayList<Integer>();
		while (rs.next()) {
			res.add(rs.getInt(1));
		}
		rs.close();
		return res;
	}

	int nbValeursPossibles(int idQuestionnaire, int numQuestion) {
		try {
			PreparedStatement ps = laConnection.mysql
					.prepareStatement("select IFNULL(count(*),0) from VALPOSSIBLE "
							+ "where idQ= ? and numQ=?");
			ps.setInt(1, idQuestionnaire);
			ps.setInt(2, numQuestion);
			ResultSet rs = ps.executeQuery();
			rs.next();
			return rs.getInt(1);
		} catch (SQLException e) {
			return 0;
		}

	}*/

	public ArrayList<String> listeValeursPossibles(int idQuestionnaire, int numQuestion) {
		ArrayList<String> res = new ArrayList<String>();
		try {
			PreparedStatement ps = laConnection.mysql
					.prepareStatement("select Valeur from VALPOSSIBLE "
							+ "where idQ= ? and numQ=?");
			ps.setInt(1, idQuestionnaire);
			ps.setInt(2, numQuestion);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				res.add(rs.getString(1));
			}
			rs.close();
			return res;

		} catch (SQLException e) {
			return null;
		}
	}

	public int getIdQuestionnaire(String titreQ) {
		int res;
		try {
			PreparedStatement ps = laConnection.mysql
					.prepareStatement("select idQ from QUESTIONNAIRE where Titre = ?");
			ps.setString(1, titreQ);
			ResultSet rs = ps.executeQuery();
			rs.next();
			res = rs.getInt(1);
			rs.close();
			return res;
		} catch (SQLException e) {
			return -1;
		}
	}

	public Coordonnees getCoordonnees() {

		try {
			Statement st = laConnection.mysql.createStatement();
			ArrayList<Sonde> interroges = new ArrayList<Sonde>();
			int idQ = -1;
			ResultSet rs = st.executeQuery("select Titre,raisonSoc,nomSond,telephoneSond,dateNaisSond,sexe,idC,numSond,idPan from CLIENT natural join CARACTERISTIQUE natural join QUESTIONNAIRE natural join SONDE where Etat='S'");

			while (rs.next()) {
				listeCoord.add(new Coordonnees(rs.getString(1),
											   rs.getString(2), 
											   rs.getString(3), 
											   rs.getString(4), 
											   rs.getString(5), 
											   rs.getString(6),
											   rs.getString(7), 
											   rs.getInt(8), 
											   rs.getInt(9)));
				if (idQ == -1) {
					idQ = this.getIdQuestionnaire(rs.getString(1));
					interroges = this.listeInterroger(rs.getInt(9), idQ);
				}
			}
			rs.close();
			for (Sonde s : interroges) {
				if (s.getNumSond() == num) {
					num = random.nextInt(listeCoord.size());
				}
			}
			return listeCoord.get(num);
		} catch (SQLException e1) {
			e1.printStackTrace();
			return null;
		}
	}

	public void setReponses(int idQ, int numQ, String idC, String valeur) {
		try {
			PreparedStatement ps = laConnection.mysql.prepareStatement("insert into REPONDRE values (?,?,?,?)");
			ps.setInt(1, idQ);
			ps.setInt(2, numQ);
			ps.setString(3, idC);
			ps.setString(4, valeur);
			ps.executeUpdate();
			ps.close();

		} catch (SQLException e) {
			System.out.println("erreur setReponses() bd");
		}
	}

	public void setInterroger(int idU, int numSond, int idQ) {
		try {
			PreparedStatement ps = laConnection.mysql.prepareStatement("insert into INTERROGER values (?,?,?)");
			ps.setInt(1, idU);
			ps.setInt(2, numSond);
			ps.setInt(3, idQ);
			ps.executeUpdate();
			ps.close();

		} catch (SQLException e) {
		}
	}

}
