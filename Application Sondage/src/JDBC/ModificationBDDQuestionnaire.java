package JDBC;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Creation.VueCreaionQuest;
import Creation.VueQuestionnairePetit;
import Creation.VueReponse;


public class ModificationBDDQuestionnaire {
	 //Attributs
	  ConnexionMySQL laConnexion=null;
	  Statement st;
	  Statement st2;
	  Statement st3;
	  Statement st4;
	  
	  //Constructeur
	  public ModificationBDDQuestionnaire(ConnexionMySQL c)throws SQLException{
		  
		  //Variables
		  laConnexion=c;
		  st = laConnexion.mysql.createStatement();
		  st2 = laConnexion.mysql.createStatement();
		  st3 = laConnexion.mysql.createStatement();
		  st4 = laConnexion.mysql.createStatement();
	  }
	  public void modificationQuestionnaire(VueQuestionnairePetit q){
		  try {
			q.vue.crea.afficherVueQuestionnaireCreation(true);
			q.vue.crea.vue.idQuestionnaire = q.idQuestionnaire;
			ResultSet ps = st.executeQuery("select * from QUESTIONNAIRE where idQ = "+String.valueOf(q.idQuestionnaire));
			ps.next();
			q.vue.crea.vue.vueInfo.titreText.setText(ps.getString("Titre"));
			ResultSet ps4 = st4.executeQuery("select * from PANEL where idPan = "+ps.getInt("IdPan"));
			ps4.next();
			q.vue.crea.vue.vueInfo.panel.setSelectedItem(ps4.getString("nomPan"));
			ps4.close();
			ps.close();
			ResultSet ps2 = st2.executeQuery("select * from QUESTION where idQ = "+String.valueOf(q.idQuestionnaire));
			while(ps2.next()){
				VueCreaionQuest question = new VueCreaionQuest(q.vue.crea.vue);
				switch(ps2.getString("idT")){
				case "u":
					question.vueType.typeReponse.setSelectedItem("Choix unique");
					ResultSet ps3 = st3.executeQuery("select * from VALPOSSIBLE where idQ = "+String.valueOf(q.idQuestionnaire)+" and numQ = "+String.valueOf(ps2.getString("numQ")));
					while(ps3.next()){
						VueReponse reponse = new VueReponse(question);
						reponse.resultat.setText(ps3.getString("Valeur"));
						question.listR.add(reponse);
					}
					ps3.close();
					break;
				case "m":
					question.vueType.typeReponse.setSelectedItem("Choix multiples");
					ResultSet ps5 = st3.executeQuery("select * from VALPOSSIBLE where idQ = "+String.valueOf(q.idQuestionnaire)+" and numQ = "+String.valueOf(ps2.getString("numQ")));
					while(ps5.next()){
						VueReponse reponse = new VueReponse(question);
						reponse.resultat.setText(ps4.getString("Valeur"));
						question.listR.add(reponse);
					}
					ps5.close();
					break;
				case "c":
					question.vueType.typeReponse.setSelectedItem("Classement");
					ResultSet ps6 = st3.executeQuery("select * from VALPOSSIBLE where idQ = "+String.valueOf(q.idQuestionnaire)+" and numQ = "+String.valueOf(ps2.getString("numQ")));
					while(ps6.next()){
						VueReponse reponse = new VueReponse(question);
						reponse.resultat.setText(ps6.getString("Valeur"));
						question.listR.add(reponse);
					}
					for(VueReponse r : question.listR){
						r.majComboBox();
					}
					ps6.close();
					break;
				case "n":
					question.vueType.typeReponse.setSelectedItem("Notes");
					break;
				case "l":
					question.vueType.typeReponse.setSelectedItem("Réponse courte");
					break;
				}
				question.majInterface();
				question.vueIntitule.titreQ.setText(ps2.getString("texteQ"));
				q.vue.crea.vue.listP.add(question);
			}
			q.vue.crea.vue.majInterface();
			st.close();
			st2.close();
			st3.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	  }
}
