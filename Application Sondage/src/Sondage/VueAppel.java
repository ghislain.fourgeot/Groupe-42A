package Sondage;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Connexion.InterfaceConnexion;
import JDBC.ModeleBD;

@SuppressWarnings("serial")
public class VueAppel extends JPanel {
	InterfaceConnexion parent;
	ModeleBD bd;

	public VueAppel(InterfaceConnexion parent) throws SQLException {
		super();
		this.parent = parent;
		this.bd = parent.bd;

		ControleurAppel ca = new ControleurAppel(this);
		this.setLayout(new BorderLayout());

		Coordonnees sondeCourant = bd.getCoordonnees();
		VueCoordonnees vCoordonnees = new VueCoordonnees(sondeCourant);

		JPanel boutons = new JPanel();
		boutons.setLayout(new FlowLayout(FlowLayout.CENTER));
		JButton ap = new JButton("Appeler");
		JLabel image = new JLabel( new ImageIcon("tel.jpg"));
		ap.setBackground(new Color(0,205,0));
		ap.setPreferredSize(new Dimension(200,30));
		ap.addActionListener(ca);
		
		boutons.add(ap);
		boutons.add(image);

		JPanel titre = new JPanel();
		titre.setLayout(new FlowLayout(FlowLayout.CENTER));
		titre.setBorder(BorderFactory.createEmptyBorder(50, 0, 0, 0));
		
		JLabel labTitre = new JLabel("Rapid'Sond");
		Font font = new Font("Arial", Font.BOLD, 50);
		labTitre.setFont(font);
		titre.add(labTitre);

		this.add(boutons, "South");
		this.add(Box.createRigidArea(new Dimension(445, 20)), "West");
		this.add(Box.createRigidArea(new Dimension(350, 20)), "East");
		this.add(titre, "North");
		this.add(vCoordonnees, "Center");
	}
}