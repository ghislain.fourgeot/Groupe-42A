package Sondage;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
@SuppressWarnings("serial")
public class VueBouton extends JPanel{
	JButton b;
	public VueBouton(String nom, Color couleur){
		super();
		this.setLayout(new FlowLayout(FlowLayout.CENTER));
		b = new JButton(nom);
		b.setBackground(couleur);
		b.setActionCommand(nom);
		this.add(b);
	}
}
