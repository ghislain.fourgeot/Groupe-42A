package Sondage;
import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class VueCoordonnees extends JPanel {
	String intituleS;
	String nomSond;
	String numTel;
	String dateNais;
	String sexeS;
	String raisonS;

	public VueCoordonnees(Coordonnees c) {
		super();
		this.intituleS = c.getIntituleS();
		this.nomSond = c.getNomSond();
		this.numTel = c.getNumTel();
		this.dateNais = c.getDateNais();
		this.raisonS = c.getRaisonS();

		if (c.getSexeS().equals("F")) {
			this.sexeS = "Femme";
		} else {
			this.sexeS = "Homme";
		}

		Font fontC = new Font("Arial", Font.BOLD, 15);
		Font fontT = new Font("Arial", Font.BOLD, 25);

		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

		JPanel infos = new JPanel();
		infos.setLayout(new BoxLayout(infos, BoxLayout.Y_AXIS));
		infos.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		infos.setBorder(BorderFactory.createTitledBorder("Coordonnées"));

		JLabel infoSo = new JLabel("Informations société");
		infoSo.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		infoSo.setFont(fontT);

		JLabel intitule = new JLabel("Intitule sondage : " + intituleS
				+ "        ");
		intitule.setBorder(BorderFactory.createEmptyBorder(0, 50, 0, 0));
		intitule.setFont(fontC);

		JLabel sc = new JLabel("Société cliente : " + raisonS);
		sc.setBorder(BorderFactory.createEmptyBorder(0, 50, 0, 0));
		sc.setFont(fontC);

		JLabel infoSon = new JLabel("Informations sondé");
		infoSon.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		infoSon.setFont(fontT);

		JLabel nomSonde = new JLabel("Nom du sondé : " + nomSond);
		nomSonde.setBorder(BorderFactory.createEmptyBorder(0, 50, 0, 0));
		nomSonde.setFont(fontC);

		JLabel num = new JLabel("N° de téléphone : " + numTel);
		num.setBorder(BorderFactory.createEmptyBorder(0, 50, 0, 0));
		num.setFont(fontC);

		JLabel date = new JLabel("Date de naissance : " + dateNais);
		date.setBorder(BorderFactory.createEmptyBorder(0, 50, 0, 0));
		date.setFont(fontC);

		JLabel sexe = new JLabel("Sexe : " + sexeS);
		sexe.setBorder(BorderFactory.createEmptyBorder(0, 50, 20, 0));
		sexe.setFont(fontC);

		infos.add(infoSo);
		infos.add(intitule);
		infos.add(sc);

		infos.add(infoSon);
		infos.add(nomSonde);
		infos.add(num);
		infos.add(date);
		infos.add(sexe);

		JLabel l = new JLabel();
		l.setBorder(BorderFactory.createEmptyBorder(1, 1, 80, 1));
		this.add(l);
		this.add(infos);
	}

}
