package Sondage;

public class Question {
	String texteQuestion;
	int numQuestion;
	int valMax;
	char typeQuestion;

	public Question(int numQuestion, String texteQuestion, int valMax, char typeQuestion) {
		super();
		this.texteQuestion = new String(texteQuestion);
		this.numQuestion=numQuestion;
		this.typeQuestion=typeQuestion;
		this.valMax=valMax;
	}

	public int getValMax() {
		return valMax;
	}

	public void setValMax(int valMax) {
		this.valMax = valMax;
	}

	public String getTexteQuestion() {
		return texteQuestion;
	}
	public void setTexteQuestion(String texteQuestion) {
		this.texteQuestion = new String(texteQuestion);
	}

	public int getNumQuestion() {
		return numQuestion;
	}

	public void setNumQuestion(int numQuestion) {
		this.numQuestion = numQuestion;
	}

	public char getTypeQuestion() {
		return typeQuestion;
	}

	public void setTypeQuestion(char typeQuestion) {
		this.typeQuestion = typeQuestion;
	}

}
