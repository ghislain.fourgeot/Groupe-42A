package Sondage;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import Connexion.InterfaceConnexion;

public class ControleurQuestionnaire implements ActionListener {
	VueQuestionnaire vueQuestionnaire;
	InterfaceConnexion fenetrePrincipale;
	int o;
	String commentaire;
	int idQ;
	int numSond;
	int idU = 0;

	ControleurQuestionnaire(VueQuestionnaire vueQuestionnaire) {
		super();
		this.vueQuestionnaire = vueQuestionnaire;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		fenetrePrincipale = (InterfaceConnexion) vueQuestionnaire.getRootPane()
				.getParent();
		
		if (arg0.getActionCommand().equals("Annuler questionnaire")) {
			o = JOptionPane.showConfirmDialog(vueQuestionnaire,
					"Etes-vous sûr de vouloir annuler le questionnaire ?",
					"ATTENTION", JOptionPane.YES_NO_OPTION);
			switch (o) {
			case 0: // OUI
				commentaire = JOptionPane.showInputDialog(vueQuestionnaire,
						"Veuillez indiquer la raison de votre annulation");
				try {
					fenetrePrincipale.bd.num += 1;
					fenetrePrincipale.afficherVueAppel();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				break;

			default:
				try {
					fenetrePrincipale.afficherVueQuestionnaireAppel();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				break;
			}

		}
		if (arg0.getActionCommand().equals("Valider")) {
			boolean sontValides = true;
			String msg = "";
			for (VueReponses rep : vueQuestionnaire.questions.lesVueRep) {
				if (!rep.estValide()) {
					sontValides = false;
					msg = "La question " + rep.q.getNumQuestion()
							+ " est mal remplie !";
					break;
				}
			}
			//Si elles sont valides on récupère les réponses de chaque question
			//et on les envoie à la bd
			if (sontValides) {
				for (VueReponses rep : vueQuestionnaire.questions.lesVueRep) {
					idQ = vueQuestionnaire.questions.idQ;
					numSond = vueQuestionnaire.coordonnees.getNumSond();
					int numQ = rep.q.getNumQuestion();
					String idC = vueQuestionnaire.coordonnees.getIdC();
					String valeur = rep.getReponse();
					
					vueQuestionnaire.bd.setReponses(idQ, numQ, idC, valeur);
				}
				//On Met à jour ensuite la table Interroger
				vueQuestionnaire.bd.setInterroger(idU, numSond, idQ);
				
				JOptionPane.showMessageDialog(vueQuestionnaire,
						"Le questionnaire a bien été envoyé !");
				fenetrePrincipale.bd.num += 1;
				try {
					fenetrePrincipale.afficherVueAppel();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				JOptionPane.showMessageDialog(vueQuestionnaire, msg);
			}
		}
	}
}
