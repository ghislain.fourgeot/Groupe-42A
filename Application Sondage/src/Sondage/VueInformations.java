package Sondage;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class VueInformations extends JPanel{
	
	public VueInformations(Coordonnees c){
		super();
		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		// partie informations du sondage
		JPanel infosSondage = new JPanel();
		infosSondage.setLayout(new BoxLayout(infosSondage,BoxLayout.Y_AXIS));
		//infosSondage.setBorder(BorderFactory.createTitledBorder("Informations sondage"));
		
		JLabel infSondage = new JLabel("Informations sondage");
		JLabel fixeIntitule = new JLabel("Intitulé du sondage : "+c.getIntituleS());
		JLabel fixeSociete = new JLabel("Société cliente : "+c.getRaisonS());
		
		infosSondage.add(infSondage);
		infosSondage.add(Box.createVerticalStrut(5));
		infosSondage.add(fixeIntitule);
		infosSondage.add(fixeSociete);
		
		// partie informations du sondé
		JPanel infosSonde = new JPanel();
		infosSonde.setLayout(new BoxLayout(infosSonde,BoxLayout.Y_AXIS));
		
		JLabel infSonde = new JLabel("Informations sondé(e)");
		JLabel nomSonde = new JLabel("Nom du sondé : "+c.getNomSond());
		
		infosSonde.add(infSonde);
		infosSonde.add(Box.createVerticalStrut(5));
		infosSonde.add(nomSonde);
		
		this.add(infosSondage);
		this.add(Box.createVerticalStrut(20));
		this.add(infosSonde);
		this.add(Box.createVerticalStrut(10));
	}	
}
