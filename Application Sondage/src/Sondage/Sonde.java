package Sondage;

public class Sonde {
	int numSond;
	String idCaracteristique;
	public Sonde(int numSond, String idCaracteristique) {
		super();
		this.numSond = numSond;
		this.idCaracteristique = idCaracteristique;
	}
	public int getNumSond() {
		return numSond;
	}
	public void setNumSond(int numSond) {
		this.numSond = numSond;
	}
	public String getIdCaracteristique() {
		return idCaracteristique;
	}
	public void setIdCaracteristique(String idCaracteristique) {
		this.idCaracteristique = idCaracteristique;
	}
	
}
