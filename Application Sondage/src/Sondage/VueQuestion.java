package Sondage;
import javax.swing.JPanel;
abstract class VueQuestion extends JPanel{
	public abstract String getReponse();
	public abstract boolean estValide();
}
