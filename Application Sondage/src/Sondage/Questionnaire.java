package Sondage;


import java.util.ArrayList;


public class Questionnaire {
	int numQuestionnaire;
	String titreQuestionnaire;
	int idPanel;
	int taillePanel;
	ArrayList <Question> listeQuestions;

	public int getNumQuestionnaire() {
		return numQuestionnaire;
	}
	public void setNumQuestionnaire(int numQuestionnaire) {
		this.numQuestionnaire = numQuestionnaire;
	}
	public Questionnaire(int numQuestionnaire, String titreQuestionnaire, int idPanel, int taillePanel) {
		super();
		this.numQuestionnaire = numQuestionnaire;
		this.titreQuestionnaire = titreQuestionnaire;
		this.idPanel=idPanel;
		this.taillePanel=taillePanel;
		this.listeQuestions = new ArrayList <Question>();
	}
	
	public int getTaillePanel() {
		return taillePanel;
	}
	public void setTaillePanel(int taillePanel) {
		this.taillePanel = taillePanel;
	}
	public String getTitreQuestionnaire() {
		return titreQuestionnaire;
	}
	public void setTitreQuestionnaire(String titreQuestionnaire) {
		this.titreQuestionnaire = titreQuestionnaire;
	}
	
	public int getIdPanel() {
		return idPanel;
	}
	public void setIdPanel(int idPanel) {
		this.idPanel = idPanel;
	}
	public ArrayList<Question> getListeQuestions() {
		return listeQuestions;
	}
	public void setListeQuestions(ArrayList<Question> listeQuestions) {
		this.listeQuestions = listeQuestions;
	}
	public void addQuestion(Question q){
		this.listeQuestions.add(q);
	}
	public void delQuestion(int ind){
		this.listeQuestions.remove(ind);
	}
	public int nbQuestions(){
		return this.listeQuestions.size();
	}
	public Question getQuestion(int ind){
		return this.listeQuestions.get(ind);
	}
	public void setQuestion(int ind,Question q){
		this.listeQuestions.set(ind, q);
	}
	public String toString(){
		return new String(this.titreQuestionnaire);
	}
}
