package Sondage;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
public class VueProgression extends JPanel{
	JLabel titre;
	VueQuestionnaire vueQuestionnaire;
	
	public VueProgression(VueQuestionnaire vueQuestionnaire){
		//panel avec la barre de progression
		this.vueQuestionnaire=vueQuestionnaire;
		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		
		JPanel progress = new JPanel();
		
		//label au dessus de la barre de progression
		titre = new JLabel("Question "+"0/6");
		
		// création de la barre de progression
		JProgressBar pb = new JProgressBar();
		pb.setMaximumSize(new Dimension(300,20));
		
		progress.setLayout(new BoxLayout(progress,BoxLayout.Y_AXIS));
		progress.add(titre);
		progress.add(pb);
		progress.setAlignmentX(LEFT_ALIGNMENT);

		this.add(progress);
	}
}
