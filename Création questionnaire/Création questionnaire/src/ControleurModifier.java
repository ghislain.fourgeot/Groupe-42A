
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;


public class ControleurModifier implements ActionListener{
	VueQuestionnairePetit vue;
	ControleurModifier(VueQuestionnairePetit vue){
		this.vue = vue;
	}
	public void actionPerformed(ActionEvent arg0) {
		vue.m.modificationQuestionnaire(vue);
		try {
			vue.connec.mysql.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}

