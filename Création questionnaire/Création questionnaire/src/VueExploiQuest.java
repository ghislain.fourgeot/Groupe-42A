
import java.awt.BorderLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class VueExploiQuest extends JPanel {
	JLabel exploi;
	JLabel nbr;
	JCheckBox exploiCheck;
	JLabel nbrQuest;
	VueExploiQuest(){
		exploi = new JLabel("Mis en exploitation :");
		nbr = new JLabel("Nombre de questions :");
		exploiCheck = new JCheckBox();
		exploiCheck.setEnabled(false);
		nbrQuest = new JLabel("");
		JPanel info_desc1 = new JPanel();
		JPanel info_desc2 = new JPanel();
		info_desc1.add(exploi);
		info_desc1.add(exploiCheck);
		info_desc2.add(nbr);
		info_desc2.add(nbrQuest);
		this.setLayout(new BorderLayout());
		this.add(info_desc1,BorderLayout.NORTH);
		this.add(info_desc2);
	}
}
