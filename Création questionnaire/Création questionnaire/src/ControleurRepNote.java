import java.awt.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class ControleurRepNote implements ChangeListener{
	VuePanelQuestions panelQ;

	ControleurRepNote(VuePanelQuestions panelQ){
		super();
		this.panelQ = panelQ;
	}
	public void stateChanged(ChangeEvent arg0) {
		JSlider source = (JSlider) arg0.getSource();
		int val = (int)source.getValue();
		System.out.println("valeur de "+ source.getName()+": "+val);

	}
}
