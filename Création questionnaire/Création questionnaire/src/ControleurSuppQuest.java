import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

//Controleur du bouton "Supprimer question"
public class ControleurSuppQuest implements ActionListener{
	
	//Attributs
	VueCreationQuestionnaire vueCrea;
	VueSuppQuest vueQuestion;
	
	//Constructeur
	ControleurSuppQuest(VueCreationQuestionnaire vue,VueCreaionQuest vueQuestion){
		
		//Variables
		this.vueCrea = vue;
	}
	
	//Suppression d'une question lors de l'appui sur le bouton
	public void actionPerformed(ActionEvent arg0) {
		int reply = JOptionPane.showConfirmDialog(((Component)arg0.getSource()).getParent(),"Etes-vous sûr de vouloir supprimer cette question ?","Confirmation de suppression",JOptionPane.YES_NO_OPTION);
		if(reply == JOptionPane.YES_OPTION){
			this.vueQuestion = (VueSuppQuest) ((Component) arg0.getSource()).getParent();
			vueCrea.supprimerQuestion(vueQuestion.vue);
			vueCrea.majInterface();
		}
	}
}