import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//Controleur du bouton "Supprimer la réponse"
public class ControleurSuppReponse implements ActionListener{
	
	//Attributs
	VueCreaionQuest vueCrea;
	VueReponse reponse;
	
	//Constructeur
	ControleurSuppReponse(VueCreaionQuest vue,VueReponse reponse){
		
		//Variables
		this.vueCrea = vue;
		this.reponse = reponse;
	}
	
	//Suppression de la réponse lors de l'appui sur le bouton
	public void actionPerformed(ActionEvent arg0) {
		this.reponse = (VueReponse) ((Component) arg0.getSource()).getParent();
		vueCrea.supprimerReponse(reponse);
		if(vueCrea.getTypeQuestion() == 3){
			for(VueReponse reponse: vueCrea.listR){
				reponse.majComboBox();
			}
		}
		vueCrea.majInterface();
	}

}
