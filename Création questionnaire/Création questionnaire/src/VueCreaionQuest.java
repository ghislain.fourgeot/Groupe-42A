
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

//Classe permettant la création d'une question
public class VueCreaionQuest extends JPanel{
	
	//Attributs
	VueCreationQuestionnaire vueQuest;
	ArrayList<VueReponse> listR;
	VueListeReponse list;
	JScrollPane scroll;
	VueAjoutReponseBouton ajouterR;
	VueTypeQuestion vueType;
	ButtonGroup groupBouttonRadio;
	ArrayList<JRadioButton> listeRadioBoutons;
	JLabel trou;
	VueSuppQuest boutonSuppQuest;
	VueIntituleQuest vueIntitule;
	
	//Constructeur
	VueCreaionQuest(VueCreationQuestionnaire vue){
		super();
		
		//Variables
		this.vueQuest = vue;
		this.boutonSuppQuest = new VueSuppQuest(vueQuest,this);
		this.trou = new JLabel();
		this.ajouterR = new VueAjoutReponseBouton(vueQuest,this);
		this.groupBouttonRadio = new ButtonGroup();
		this.listeRadioBoutons = new ArrayList<JRadioButton>();
		this.vueType = new VueTypeQuestion(this);
		this.listR = new ArrayList<VueReponse>();
		this.vueIntitule = new VueIntituleQuest();
		
		//Paramètres de la fenêtre
		this.setLayout(new GridLayout(3,2));
		this.setBorder(BorderFactory.createLineBorder(Color.black,8));
		this.setSize(200,100);
		this.setPreferredSize(new Dimension(200,400));
		this.setMaximumSize(new Dimension(200,100));
		
		//Ajout d'élements à la fenêtre
		this.add(vueIntitule);
		this.add(vueType);
		this.list = new VueListeReponse(listR);
		this.scroll = AjoutScroll(list);
		this.add(scroll);
		this.add(ajouterR);
		this.add(trou);
		this.add(boutonSuppQuest,BorderLayout.WEST);
		}
	
	//Ajouter une réponse à la question
	public void ajouteReponse(){
		this.listR.add(new VueReponse(this));
	}
	
	//Supprimer une réponse à la question
	public void supprimerReponse(VueReponse reponse){
		this.listR.remove(reponse);
	}
	
	//Récuperer le type de la question
	public int getTypeQuestion(){
		int res = vueType.typeReponse.getSelectedIndex(); 
		return res;
	}
	
	//Création de la liste déroulante des réponses
	private JScrollPane AjoutScroll(VueListeReponse vue){
		JScrollPane res = new JScrollPane(vue);
		res.setSize(new Dimension(500,400));
		res.getVerticalScrollBar().setUnitIncrement(16);
		return res;
	}
	
	//Rafraichissement de la fenêtre
	public void majInterface(){
		this.groupBouttonRadio = new ButtonGroup();
		for(JRadioButton b : listeRadioBoutons){
			this.groupBouttonRadio.add(b);
		}
		this.remove(scroll);
		this.remove(ajouterR);
		this.remove(trou);
		this.remove(boutonSuppQuest);
		this.list = new VueListeReponse(listR);
		scroll = AjoutScroll(list);
		this.add(scroll);
		this.add(ajouterR);
		this.add(trou);
		this.add(boutonSuppQuest,BorderLayout.WEST);
		this.revalidate();
		this.repaint();
	}
	
	//Ajout d'un JRadioButton au groupe de boutons
	public void ajoutRadioButton(JRadioButton b){
		this.listeRadioBoutons.add(b);
	}
}
