import java.awt.*;
import java.sql.SQLException;
import javax.swing.*;


@SuppressWarnings("serial")
public class VueQuestionnaireFixe extends JPanel{
	
	VueInformations vi;
	VueBouton bAnnuler;
	VueQuestionnaire parent;

	public VueQuestionnaireFixe(VueQuestionnaire parent) throws SQLException{
		super();
		this.parent = parent;
		// jpanel de droite informations sondage et sondé
		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createTitledBorder("Informations"));
		
		
		// panel pour créer un bloc vide 
		JPanel comble = new JPanel();
		comble.add(new JLabel("            "));
		comble.add(Box.createRigidArea(new Dimension(200,200)));
		//comble.setBorder(BorderFactory.createTitledBorder("comble"));
		
		// panel avec les infos du sondage et du sonde
		Coordonnees sondeCourant = parent.bd.getCoordonnees();
		vi = new VueInformations(sondeCourant);
		//vi.setBorder(BorderFactory.createTitledBorder("vi"));
		
		/*panel barre de progression
		vp = new VueProgression(parent);
		progression.setBorder(BorderFactory.createTitledBorder("progression"));
		*/
		
		//panel qui gère le bouton annuler questionnaire
		bAnnuler = new VueBouton("Annuler questionnaire",Color.RED);
		
		JPanel centre = new JPanel();
		centre.setLayout(new BoxLayout(centre,BoxLayout.Y_AXIS));
		centre.add(vi);
		centre.add(Box.createRigidArea(new Dimension(20,30)),"Center");
		
		int nbQ = parent.lesquestions.size();
		JLabel labNbQ = new JLabel("Nombre de Questions : "+nbQ);
		centre.add(labNbQ);
		//centre.add(vp);
		
		//Ajout au panel infos
		this.add(comble,"North");
		this.add(bAnnuler,"South");
		this.add(centre,"Center");
	}
}
