
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.JPanel;


public class VueQuestionnairePetit extends JPanel {
	VueTitreDescPetitQuest vueHaut;
	VueBoutonPetitQuest vueBas;
	int idQuestionnaire;
	SupprimerBDDQuestionnaire r;
	ModificationBDDQuestionnaire m;
	VueCreationListeQuestionnaire vue;
	ConnexionMySQL connec;
	VueQuestionnairePetit(VueCreationListeQuestionnaire vue){
		connec =new ConnexionMySQL();
		try {
			r = new SupprimerBDDQuestionnaire(connec);
			m = new ModificationBDDQuestionnaire(connec);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.vue = vue;
		this.vueHaut = new VueTitreDescPetitQuest();
		this.vueBas = new VueBoutonPetitQuest(this);
		this.setLayout(new BorderLayout());
		this.add(vueHaut,BorderLayout.NORTH);
		this.add(vueBas,BorderLayout.CENTER);
		this.setPreferredSize(new Dimension(800,100));
		this.setBorder(BorderFactory.createLineBorder(Color.black,1));
	}
}
