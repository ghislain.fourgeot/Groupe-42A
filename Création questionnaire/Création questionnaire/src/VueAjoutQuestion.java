import javax.swing.JButton;
import javax.swing.JPanel;

//Panel du bouton "Ajouter question"
public class VueAjoutQuestion extends JPanel{
	
	//Attributs
	VueCreationQuestionnaire vueCrea;
	
	//Constructeur
	VueAjoutQuestion(VueCreationQuestionnaire vue){
		super();
		
		//Variables
		this.vueCrea = vue;
		
		JButton ajouter = new JButton("Ajouter question");
		ajouter.addActionListener(new ControleurAjoutQuest(vue));
		this.add(ajouter);
	}
}
