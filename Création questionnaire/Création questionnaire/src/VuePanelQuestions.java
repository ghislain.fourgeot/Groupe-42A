
import java.util.ArrayList;
import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class VuePanelQuestions extends JPanel {
	ArrayList<VueReponses> lesVueRep;
	ArrayList<Question> lesQuestions;
	ArrayList<String> repPossibles;
	
	String type = "";
	int idQ;
	VueQuestionnaire parent;
	ModeleBD bd;

	public VuePanelQuestions(VueQuestionnaire parent,ArrayList<Question> lesQuestions,int idQ) {
		super();
		lesVueRep = new ArrayList<VueReponses>();
		this.parent = parent;
		this.bd = parent.bd;
		this.idQ = idQ;
		this.lesQuestions= lesQuestions;
		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));

		for (Question q : lesQuestions){
			JPanel panQ = new JPanel();
			panQ.setLayout(new BoxLayout(panQ,BoxLayout.Y_AXIS));

			JPanel panNomQ = new JPanel();
			panNomQ.setLayout(new FlowLayout(FlowLayout.CENTER));
			panNomQ.add(new JLabel(q.getTexteQuestion()));

			panQ.add(panNomQ);
			panQ.add(this.addQuestion(q));
			panQ.setBorder(BorderFactory.createTitledBorder(q.getNumQuestion()+". "+type));
			this.add(panQ);
		}
	}



	JPanel addQuestion(Question q){
		int numQuestion = q.getNumQuestion();
		repPossibles = this.bd.listeValeursPossibles(idQ, numQuestion);
		VueReponses vueReponses = new VueReponses(q,repPossibles);
		lesVueRep.add(vueReponses);
		type = vueReponses.type;
		return vueReponses;
	}
}
