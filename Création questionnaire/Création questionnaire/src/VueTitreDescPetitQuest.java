
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class VueTitreDescPetitQuest extends JPanel {
	JLabel titre;
	JLabel desc;
	JLabel titreText;
	JLabel descText;
	VueExploiQuest vueExploi;
	VueTitreDescPetitQuest(){
		titre = new JLabel("Intitulé du questionnaire : ");
		desc = new JLabel("");
		titreText = new JLabel();
		descText = new JLabel();
		vueExploi = new VueExploiQuest();
		JPanel info_desc1 = new JPanel();
		JPanel info_desc2 = new JPanel();
		JPanel tout = new JPanel();
		info_desc1.add(titre);
		info_desc1.add(titreText);
		info_desc2.add(desc);
		info_desc2.add(descText);
		tout.setLayout(new BorderLayout());
		tout.add(info_desc1,BorderLayout.NORTH);
		tout.add(info_desc2);
		this.add(tout);
		this.add(vueExploi);
	}
}
