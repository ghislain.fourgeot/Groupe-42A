
import java.sql.*;

public class RecuperationBDDQuestionnaire {
	
	  //Attributs
	  ConnexionMySQL laConnexion=null;
	  Statement st;
	  Statement st2;
	  Statement st3;
	  
	  //Constructeur
	  RecuperationBDDQuestionnaire(ConnexionMySQL c)throws SQLException{
		  
		  //Variables
		  laConnexion=c;
		  st = laConnexion.mysql.createStatement();
		  st2 = laConnexion.mysql.createStatement();
		  st3 = laConnexion.mysql.createStatement();
	  }
	  //Ajout du questionnaire à la base de donnée
	  public void ajoutListe(VueCreationListeQuestionnaire questionnaire) throws SQLException{
		  int id = 0;
		  try{
			  ResultSet rs = st.executeQuery("select * from QUESTIONNAIRE");
			  while(rs.next()){
				  if(rs.getString("Etat") != "A"){
					  id = rs.getInt("idQ");
					  ResultSet ah = st2.executeQuery("select * from QUESTIONNAIRE where idQ = "+String.valueOf(id));
					  ah.next();
					  VueQuestionnairePetit q = new VueQuestionnairePetit(questionnaire);
					  q.vueHaut.titreText.setText(ah.getString(2));
					  if(ah.getString(3).equals("S")){
						  q.vueHaut.vueExploi.exploiCheck.setSelected(true);
						  q.vueBas.exploi.setEnabled(false);
						  q.vueBas.modif.setEnabled(false);
					  }
					  ResultSet bh = st3.executeQuery("select max(numQ) from QUESTION where idQ = "+String.valueOf(id));
				   	  bh.next();
				  	  q.vueHaut.vueExploi.nbrQuest.setText(String.valueOf(bh.getInt(1)));
				  	  questionnaire.listQ.add(q);
				  	  q.idQuestionnaire =id;
				  }
			  }
			  st.close();
			  st2.close();
			  st3.close();
		  }
		  catch(SQLException e){
			  System.out.println("Pas de table ");
			  e.printStackTrace();
		  }
	  }
}