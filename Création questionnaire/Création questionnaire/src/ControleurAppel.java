import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JButton;
import javax.swing.JOptionPane;

public class ControleurAppel implements ActionListener {
	VueAppel vueAppel;
	InterfaceConnexion parent;

	public ControleurAppel(VueAppel vueAppel) {
		super();
		this.vueAppel = vueAppel;
		this.parent = vueAppel.parent;
	}

	public void actionPerformed(ActionEvent arg0) {
		JButton b = (JButton) arg0.getSource();
		appeler(b);
	}

	public void appeler(JButton b) {
		String[] choix = { "Raccrocher", "Afficher questionnaire" };

		if (b.getText().equals("Appeler")) {
			int rep = JOptionPane.showOptionDialog(vueAppel, "Appel en cours",
					"Appel", JOptionPane.DEFAULT_OPTION,
					JOptionPane.QUESTION_MESSAGE, null, choix, choix[0]);

			if (rep == 0) {
				int rep2 = JOptionPane.showConfirmDialog(vueAppel,
						"Etes-vous sûr de vouloir raccrocher ?", "Attention",
						JOptionPane.YES_NO_OPTION);
				if (rep2 == JOptionPane.YES_OPTION) {
					try {
						parent.bd.num += 1;
						parent.afficherVueAppel();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}

				else {
					try {
						parent.afficherVueAppel();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			} else if (rep == 1) {
				try {
					parent.afficherVueQuestionnaireAppel();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
