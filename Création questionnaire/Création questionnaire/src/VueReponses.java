import java.util.ArrayList;
import java.awt.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class VueReponses extends JPanel {
	Question q;

	int nbRep;
	String type;

	JLabel lab;
	JPanel p;

	// Les réponses seront récupérés dans chaques type de boutons:
	JTextField jtf;
	ArrayList<String> repPossibles;
	ArrayList<JCheckBox> repMultiple;
	JCheckBox chb;
	ArrayList<JComboBox<String>> repClassement;
	JComboBox<String> cb;
	JRadioButton[] listeRadioBoutons;
	JSlider slider;

	// Pour chaque type de question on appel la méthode qui ajoute au JPanel les
	// boutons correspondant
	public VueReponses(Question q, ArrayList<String> repPossibles) {
		super();
		this.q = q;
		this.repPossibles = repPossibles;
		switch (q.typeQuestion) {
		case 'u': // choix unique
			this.creerRepChoixUnique();
			type = "Question à choix unique";
			break;
		case 'm': // choix multiple
			this.creerRepMultiple();
			type = "Question à choix multiples";
			break;
		case 'c': // classement
			this.creerRepClassement();
			type = "Classement par ordre de préférence";
			break;
		case 'l': // choix libre
			this.creerRepLibre();
			type = "Réponse courte";
			break;
		case 'n': // note
			this.creerRepNote();
			type = "Question notée";
		}
	}

	public void creerRepLibre() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		jtf = new JTextField(30);
		jtf.setMaximumSize(new Dimension(600, 25));
		this.add(jtf);
	}

	public void creerRepMultiple() {
		repMultiple = new ArrayList<JCheckBox>();
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		for (String rep : repPossibles) {
			chb = new JCheckBox(rep);
			this.add(chb);
			repMultiple.add(chb);
		}
	}

	public void creerRepClassement() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		repClassement = new ArrayList<JComboBox<String>>();
		for (String rep : repPossibles) {
			p = new JPanel();
			p.setLayout(new FlowLayout());
			lab = new JLabel(rep);
			cb = new JComboBox<String>();
			cb.setName(rep);
			cb.addItem(" ");
			for (int k = 1; k < repPossibles.size() + 1; k++) {
				cb.addItem(String.valueOf(k));
			}
			p.add(lab);
			p.add(cb);
			// Ajout des ComboBox dans une liste afin de récuperer les réponses
			repClassement.add(cb);
			this.add(p);
		}
	}

	public void creerRepChoixUnique() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		ButtonGroup choixRep = new ButtonGroup();
		listeRadioBoutons = new JRadioButton[repPossibles.size()];
		for (int i = 0; i < repPossibles.size(); i++) {
			listeRadioBoutons[i] = new JRadioButton(repPossibles.get(i));
			this.add(listeRadioBoutons[i]);
			choixRep.add(listeRadioBoutons[i]);
		}
	}

	public void creerRepNote() {
		int min = 0;
		int max = 10;
		slider = new JSlider();
		slider = new JSlider(JSlider.HORIZONTAL, min, max, min);
		slider.setMajorTickSpacing(1);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		this.add(slider);
	}

	// Cette méthode récupère la réponse en fonction du type de la question
	public String getReponse() {
		String res = "";
		if (type.equals("Question à choix unique")) {
			for (int i = 0; i < listeRadioBoutons.length; i++) {
				if (listeRadioBoutons[i].isSelected()) {
					res += i + 1;
				}
			}
			return res;
		}
		else if (type.equals("Question notée"))
			return String.valueOf(slider.getValue());

		else if (type.equals("Question à choix multiples")) {
			for (int i = 0; i < repMultiple.size(); i++) {
				if (repMultiple.get(i).isSelected()) {
					res += i + 1 + ";";
				}
			}
			return res;
		}
		else if (type.equals("Classement par ordre de préférence")) {
			for (int i = 1; i < repClassement.size() + 1; i++) {
				for (JComboBox<String> cb : repClassement) {
					if (cb.getSelectedIndex() == i) {
						res += repClassement.indexOf(cb) + 1 + ";";
					}
				}
			}
			return res;
		}

		else if (type.equals("Réponse courte")) {
			return jtf.getText();
		} else {
			return "erreur";
		}
	}
	
	//Cette méthode vérifie si le sondeur a bien donné sa réponse 
	public boolean estValide() {
		boolean res = false;
		//Si c'est une question noté on ne peut pas vérifier s'il a ou pas répondu
		if (type.equals("Question notée"))
			res = true;
		else if (!getReponse().equals(""))
			res = true;
		return res;
	}
}
