
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

//Classe de création de l'interface d'un questionnaire
public class VueCreationQuestionnaire extends JPanel {
	
	//Attributs
	ArrayList<VueCreaionQuest> listP;
	VueListeQuestion list;
	JScrollPane scroll;
	VueInformationsCreation vueInfo;
	char etat;
	int idQuestionnaire;
	InterfaceConnexion crea;
	InsertionBDDNouveauQuestionnaire r;
	SupprimerBDDQuestionnaire s;
	ConnexionMySQL connec;
	boolean enModif;
	//Constructeur
	public VueCreationQuestionnaire(InterfaceConnexion crea) throws SQLException {
		super();
		
		//Variables
		this.connec = new ConnexionMySQL();
		r = new InsertionBDDNouveauQuestionnaire(connec);
		s = new SupprimerBDDQuestionnaire(connec);
		this.crea = crea;
		this.listP = new ArrayList<VueCreaionQuest>();
		this.vueInfo = new VueInformationsCreation();
		this.setLayout(new BorderLayout());
		this.add(new VueEnrSupp(this),BorderLayout.NORTH);
		this.add(vueInfo,BorderLayout.WEST);
		this.add(new VueAjoutQuestion(this),BorderLayout.EAST);
		this.list = new VueListeQuestion(listP);
		this.scroll = AjoutScroll(list);
		this.add(scroll);
	}
	
	//Ajout d'une question
	public void ajoutQuestion(){
		listP.add(0,new VueCreaionQuest(this));
	}
	
	//Suppression d'une question
	public void supprimerQuestion(VueCreaionQuest question){
		listP.remove(question);
	}
	
	//Rafraichissement de l'interface
	public void majInterface(){
		this.removeAll();
		this.add(new VueEnrSupp(this),BorderLayout.NORTH);
		this.add(vueInfo,BorderLayout.WEST);
		this.add(new VueAjoutQuestion(this),BorderLayout.EAST);
		list = new VueListeQuestion(listP);
		scroll = AjoutScroll(list);
		this.add(scroll);
		this.repaint();
		this.validate();
		this.revalidate();
	}
	
	//Création de la liste déroulante des questions
	private JScrollPane AjoutScroll(VueListeQuestion vue){
		JScrollPane res = new JScrollPane(vue);
		if(listP.size() == 0){
			res.setBorder(BorderFactory.createLineBorder(Color.black,8));
		}
		res.getVerticalScrollBar().setUnitIncrement(16);
		return res;
		
	}
}
