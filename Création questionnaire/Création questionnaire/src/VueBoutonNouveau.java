
import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;


public class VueBoutonNouveau extends JPanel {
	JButton nouveau;
	VueCreationListeQuestionnaire vue;
	VueBoutonNouveau(VueCreationListeQuestionnaire vue){
		this.vue = vue;
		this.setLayout(new BorderLayout());
		this.add(Box.createRigidArea(new Dimension(500, 0)),BorderLayout.WEST);
		this.add(Box.createRigidArea(new Dimension(500, 0)),BorderLayout.EAST);
		this.add(Box.createRigidArea(new Dimension(0, 50)),BorderLayout.NORTH);
		this.add(Box.createRigidArea(new Dimension(0, 50)),BorderLayout.SOUTH);
		nouveau = new JButton("Nouveau questionnaire");
		nouveau.addActionListener(new ControleurNouveauQuestionnaire(vue.crea));
		this.add(nouveau);
	}
}
