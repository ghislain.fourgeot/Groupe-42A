
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JOptionPane;


public class ControleurSuppQuestionnaireListe implements ActionListener{
	VueQuestionnairePetit vue;
	ControleurSuppQuestionnaireListe(VueQuestionnairePetit vue){
		this.vue = vue;
	}

	public void actionPerformed(ActionEvent arg0){
		int reply = JOptionPane.showConfirmDialog(((Component)arg0.getSource()).getParent(),"Voulez-vous mettre supprimer définitivement ce questionnaire ?","Attention !",JOptionPane.YES_NO_OPTION);
		if(reply == JOptionPane.YES_OPTION){
			vue.r.supprimerQuestionnaire(vue);
			try {
				vue.connec.mysql.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			vue.vue.majInterface();
		}
		
	}

}
