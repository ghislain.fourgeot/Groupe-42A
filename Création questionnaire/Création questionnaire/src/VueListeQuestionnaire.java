
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;

//Panel de la liste des questions
public class VueListeQuestionnaire extends JPanel {
	
	//Atributs
	ArrayList<VueQuestionnairePetit> list;
	
	//Constructeur
	VueListeQuestionnaire(ArrayList<VueQuestionnairePetit> list){
		
		//Variables
		this.list = list;
		
		this.setLayout(new GridLayout(list.size(),1));
		for(VueQuestionnairePetit questionnaire : list){
			this.add(questionnaire);
		}
		this.setSize(400,200);
	}
}
