import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JOptionPane;

//Controleur du bouton "Supprimer question"
public class ControleurEnregistrer implements ActionListener{
	
	//Attributs
	VueCreationQuestionnaire vueCrea;
	VueSuppQuest vueQuestion;
	
	//Constructeur
	ControleurEnregistrer(VueCreationQuestionnaire vue){
		
		//Variables
		this.vueCrea = vue;
	}
	
	//Suppression d'une question lors de l'appui sur le bouton
	public void actionPerformed(ActionEvent arg0) {
		int reply = JOptionPane.showConfirmDialog(((Component)arg0.getSource()).getParent(),"Voulez-vous mettre ce questionnaire en exploitation ?","Enregistrement",JOptionPane.YES_NO_OPTION);
		if(reply == JOptionPane.YES_OPTION){
			vueCrea.etat = 'S';
		}
		else{
			vueCrea.etat = 'C';
		}
		try {
			if(vueCrea.enModif){
				vueCrea.s.supprimerQuestionnaireModif(vueCrea);
			}
			vueCrea.r.nouveauQuestionnaire(vueCrea);
			vueCrea.r.nouvelleQuestion(vueCrea);
			vueCrea.r.nouvelleValPoss(vueCrea);
			vueCrea.connec.mysql.close();
			vueCrea.crea.afficherVueListeQuestionnaire();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}