import java.awt.*;
import javax.swing.*;
abstract class VueQuestion extends JPanel{
	public abstract String getReponse();
	public abstract boolean estValide();
}
