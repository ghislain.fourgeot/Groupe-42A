
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;

//Panel du bouton "Supprimer question"
public class VueSuppQuest extends JPanel {
	
	//Attributs
	VueCreationQuestionnaire vueQuest;
	VueCreaionQuest vue;
	JButton supprimerQuestion;
	
	//Constructeur
	VueSuppQuest(VueCreationQuestionnaire vueQuest, VueCreaionQuest vue){
		
		//Variables
		this.vueQuest = vueQuest;
		this.vue = vue;
		this.setLayout(new BorderLayout());
		this.supprimerQuestion = new JButton("Supprimer question");
		supprimerQuestion.addActionListener(new ControleurSuppQuest(vueQuest,vue));
		supprimerQuestion.setBackground(new Color(227, 0, 0));
		this.add(Box.createRigidArea(new Dimension(62, 0)),BorderLayout.WEST);
		this.add(Box.createRigidArea(new Dimension(57, 0)),BorderLayout.EAST);
		this.add(Box.createRigidArea(new Dimension(0, 50)),BorderLayout.NORTH);
		this.add(Box.createRigidArea(new Dimension(0, 50)),BorderLayout.SOUTH);
		this.add(supprimerQuestion);
	}
	
}
