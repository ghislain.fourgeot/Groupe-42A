
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JTextField;

//Panel de création d'une réponse
public class VueReponse extends JPanel {
	
	//Attributs
	VueCreaionQuest vue;
	JComboBox classement;
	ArrayList<String> list;
	JTextField resultat;
	JCheckBox resultatCheck;
	
	//Constructeur
	VueReponse(VueCreaionQuest vue){
		super();
		
		//Variables
		this.vue = vue;
		this.list = new ArrayList<String>();
		this.setLayout(new FlowLayout());
		this.resultat = new JTextField(10);
		this.resultatCheck = new JCheckBox();
		
		switch(vue.getTypeQuestion()){
		case 0:
			this.add(resultatCheck);
			this.add(resultat);
			break;
		case 1:
			JRadioButton b = new JRadioButton();
			this.add(b);
			this.vue.ajoutRadioButton(b);
			this.add(resultat);
			break;
		case 2:
			this.add(resultat);
			break;
		case 3:
			list.clear();
			for(int i = 1; i <= vue.listR.size(); i++){
				list.add(String.valueOf(i));
			}
			String[] valeurs = new String[list.size()];
			valeurs = list.toArray(valeurs);
			classement = new JComboBox(valeurs);
			this.add(resultat);
			this.add(classement);
			break;
		case 4:
			JSlider slider = new JSlider(JSlider.HORIZONTAL,0,10,0);
			slider.setMajorTickSpacing(1);
			slider.setPaintTicks(true);
			slider.setPaintLabels(true);
			slider.setPreferredSize(new Dimension(350,40));
			this.add(slider);
			break;
		}
		JButton supprimer = new JButton("Supprimer la réponse");
		supprimer.addActionListener(new ControleurSuppReponse(vue,this));
		supprimer.setBackground(new Color(255, 128, 0));
		if(vue.getTypeQuestion() != 4 && vue.getTypeQuestion() != 2){
			this.add(supprimer);
		}
		if(vue.getTypeQuestion() == 4 || vue.getTypeQuestion() == 2){
			this.setPreferredSize(new Dimension(30,60));	
		}
		else{
			this.setPreferredSize(new Dimension(30,30));
		}
	}
	
	//Recréation de la JComboBox d'une question de type "classement"
	public void majComboBox(){
		list.clear();
		this.removeAll();
		for(int i = 1; i <= vue.listR.size(); i++){
			list.add(String.valueOf(i));
		}
		String[] valeurs = new String[list.size()];
		valeurs = list.toArray(valeurs);
		classement = new JComboBox(valeurs);
		this.add(classement);
		JButton supprimer = new JButton("Supprimer la réponse");
		supprimer.addActionListener(new ControleurSuppReponse(vue,this));
		supprimer.setBackground(new Color(255, 128, 0));
		this.add(resultat);
		this.add(supprimer);
		this.setPreferredSize(new Dimension(30,30));	
	}
}
