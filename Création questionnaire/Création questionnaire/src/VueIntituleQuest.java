
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

//Panel du nom de la question
public class VueIntituleQuest extends JPanel {
	
	//Attributs
	JTextField titreQ;
	
	//Constructeur
	VueIntituleQuest(){
		
		//Variables
		this.titreQ = new JTextField();
		
		this.setLayout(new FlowLayout());
		JLabel intituleQ = new JLabel("Choix intitulé question : ");
		titreQ.setPreferredSize(new Dimension(300,24));
		this.add(intituleQ);
		this.add(titreQ);
	}
}
