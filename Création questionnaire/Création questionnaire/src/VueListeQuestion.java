
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;

//Panel de la liste des questions
public class VueListeQuestion extends JPanel {
	
	//Atributs
	ArrayList<VueCreaionQuest> list;
	
	//Constructeur
	VueListeQuestion(ArrayList<VueCreaionQuest> list){
		
		//Variables
		this.list = list;
		
		this.setLayout(new GridLayout(list.size(),1));
		for(VueCreaionQuest question : list){
			this.add(question);
		}
	}
}
