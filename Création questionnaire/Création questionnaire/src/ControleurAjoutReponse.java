
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//Controleur du bouton "Ajouter nouvelle réponse"
public class ControleurAjoutReponse implements ActionListener{
	
	//Attributs
	VueCreaionQuest vueCreaQ;
	VueCreationQuestionnaire vueCrea;
	
	//Constructeur
	ControleurAjoutReponse(VueCreaionQuest vue, VueCreationQuestionnaire vueCrea){
		
		///Variables
		this.vueCreaQ = vue;
		this.vueCrea = vueCrea;
	}
	
	//Ajout d'une réponse lors de l'appui sur le bouton
	public void actionPerformed(ActionEvent arg0) {
		vueCreaQ.ajouteReponse();
		if(vueCreaQ.getTypeQuestion() == 3){
			for(VueReponse reponse: vueCreaQ.listR){
				reponse.majComboBox();
			}
		}
		vueCreaQ.majInterface();
	}

}
