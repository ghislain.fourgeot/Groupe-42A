
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


public class VueCreationListeQuestionnaire extends JPanel {
	ArrayList<VueQuestionnairePetit> listQ;
	VueListeQuestionnaire vueListQ;
	JScrollPane scroll;
	InterfaceConnexion crea;
	VueBoutonNouveau nouveau;
	ConnexionMySQL connec;
	VueCreationListeQuestionnaire(InterfaceConnexion crea)throws SQLException{
		listQ = new ArrayList<VueQuestionnairePetit>();
		try{
			connec = new ConnexionMySQL();
			RecuperationBDDQuestionnaire r = new RecuperationBDDQuestionnaire(connec);
			r.ajoutListe(this);
			connec.mysql.close();
		}
		catch(SQLException e){
			System.out.println("Erreur dans la base de donnée");
		}
		this.crea = crea;
		this.nouveau = new VueBoutonNouveau(this);
		vueListQ = new VueListeQuestionnaire(listQ);
		scroll = AjoutScroll(vueListQ);
		this.setLayout(new BorderLayout());
		this.add(nouveau,BorderLayout.NORTH);
		this.add(scroll);
		
	}
	private JScrollPane AjoutScroll(VueListeQuestionnaire vue){
		JScrollPane res = new JScrollPane(vue);
		res.getVerticalScrollBar().setUnitIncrement(16);
		res.setSize(new Dimension(600,400));
		return res;
	}
	public void majInterface(){
		this.removeAll();
		listQ.clear();
		try{
			connec.mysql.close();
			RecuperationBDDQuestionnaire r = new RecuperationBDDQuestionnaire(new ConnexionMySQL());
			r.ajoutListe(this);
		}
		catch(SQLException e){
			System.out.println("Erreur dans la base de donnée");
		}
		this.crea = crea;
		this.nouveau = new VueBoutonNouveau(this);
		vueListQ = new VueListeQuestionnaire(listQ);
		scroll = AjoutScroll(vueListQ);
		this.setLayout(new BorderLayout());
		this.add(nouveau,BorderLayout.NORTH);
		this.add(scroll);
		this.repaint();
		this.validate();
		this.revalidate();
	}
}
