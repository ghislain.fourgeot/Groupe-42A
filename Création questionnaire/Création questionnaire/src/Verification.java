
import java.sql.*;

public class Verification{
  ConnexionMySQL laConnexion;
  Statement st;
  Verification(ConnexionMySQL laConnexion){
    this.laConnexion=laConnexion;
    try{
      this.st=laConnexion.mysql.createStatement();
    }
    catch(SQLException e){
      System.out.println("Echec de connexion");
      System.out.println(e.getMessage());
    }
  }
//Permet de vérifier si NumC, clé étrangère de la table QUESTIONNAIRE existe dans CLIENT
  private boolean verifnumCQuest(int numClient){
    try{
      ResultSet rs = st.executeQuery("select numC from CLIENT where numC=numClient");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
//Permet de vérifier si idU, clé étrangère de la table QUESTIONNAIRE existe dans UTILISATEUR
  private boolean verifidUQuest(int idUtilisateur){
    try{
      ResultSet rs = st.executeQuery("select idU from UTILISATEUR where idU=idUtilisateur");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
//Permet de vérifier si idPan, clé étrangère de la table QUESTIONNAIRE existe dans PANEL
  private boolean verifidPanQuest(int idPanel){
    try{
      ResultSet rs = st.executeQuery("select idPan from PANEL where idPan=idPanel");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
//Permet de vérifier si numSond, clé étrangère de la table CONSTITUER existe dans SONDE
  private boolean verifidnumSondConst(int numSonde){
    try{
      ResultSet rs = st.executeQuery("select numSond from SONDE where numSond=numSonde");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
//Permet de vérifier si idPan, clé étrangère de la table CONSTITUER existe dans PANEL
  private boolean verifidPanConst(int idPanel){
    try{
      ResultSet rs = st.executeQuery("select idPan from PANEL where idPan=idPanel");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
//Permet de vérifier si idU, clé étrangère de la table INTERROGER existe dans UTILISATEUR
  private boolean verifidUInterro(int idUtilisateur){
    try{
      ResultSet rs = st.executeQuery("select idU from UTILISATEUR where idu=idUtilisateur");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
//Permet de vérifier si numSond, clé étrangère de la table INTERROGER existe dans SONDE
  private boolean verifnumSondInterro(int numSonde){
    try{
      ResultSet rs = st.executeQuery("select numSond from SONDE where numSond=numSonde");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
//Permet de vérifier si idQ, clé étrangère de la table INTERROGER existe dans QUESTIONNAIRE
  private boolean verifidQInterro(int idQuestionnaire){
    try{
      ResultSet rs = st.executeQuery("select idQ from QUESTIONNAIRE where idQ=idQuestionnaire");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
//Permet de vérifier si idC, clé étrangère de la table SONDE existe dans CARACTERISTIQUE
  private boolean verifidCSonde(char idCaracteristique){
    try{
      ResultSet rs = st.executeQuery("select idC from CARACTERISTIQUE where idC=idCaracteristique");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
//Permet de vérifier si idR, clé étrangère de la table UTILISATEUR existe dans ROLEUTIL
  private boolean verifidRUtili(char idRole){
    try{
      ResultSet rs = st.executeQuery("select idR from ROLEUTIL where idR=idRole");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
//Permet de vérifier si idQ, clé étrangère de la table VALPOSSIBLE existe dans QUESTION
  private boolean verifidQValPos(int idQuestionnaire){
    try{
      ResultSet rs = st.executeQuery("select idQ from QUESTION where idQ=idQuestionnaire");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
//Permet de vérifier si idT, clé étrangère de la table QUESTION existe dans TYPEQUESTION
  private boolean verifididTQuestion(char idTypeQuestion){
    try{
      ResultSet rs = st.executeQuery("select idT from TYPEQUESTION where idT=idTypeQuestion");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
//Permet de vérifier si idQ, clé étrangère de la table QUESTION existe dans QUESTIONNAIRE
  private boolean verifidQQuestion(int idQuestionnaire){
    try{
      ResultSet rs = st.executeQuery("select idQ from QUESTIONNAIRE where idQ=idQuestionnaire");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
//Permet de vérifier si idC, clé étrangère de la table REPONDRE existe dans CARACTERISTIQUE
  private boolean verifidCRepon(char idCaracteristique){
    try{
      ResultSet rs = st.executeQuery("select idC from CARACTERISTIQUE where idC=idCaracteristique");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
//Permet de vérifier si idQ, clé étrangère de la table REPONDRE existe dans QUESTION
  private boolean verifidQRepon(int idQuestionnaire){
    try{
      ResultSet rs = st.executeQuery("select idQ from QUESTION where idQ=idQuestionnaire");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
//Permet de vérifier si idTr, clé étrangère de la table CARACTERISTIQUE existe dans TRANCHE
  private boolean verifidTrCarac(char idTranche){
    try{
      ResultSet rs = st.executeQuery("select idTr from TRANCHE where idTr=idTranche");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
//Permet de vérifier si idCat, clé étrangère de la table CARACTERISTIQUE existe dans CATEGORIE
  private boolean verifidCatCarac(char idCategorie){
    try{
      ResultSet rs = st.executeQuery("select idCat from CATEGORIE where idCat=idCategorie");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
//Permet de vérifier si idnumQ, clé étrangère de la table VALPOSSIBLE existe dans QUESTION
  private boolean verifidnumQValPos(int idnumQuestion){
    try{
      ResultSet rs = st.executeQuery("select idNumQ from QUESTION where idnumQ=idnumQuestion");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
//Permet de vérifier si idnumQ, clé étrangère de la table REPONDRE existe dans QUESTION
  private boolean verifidnumQRepon(int idnumQuestion){
    try{
      ResultSet rs = st.executeQuery("select idnumQ from QUESTION where idnumQ=idnumQuestion");
      if (rs==null){
        return false;
      }
    }
    catch(SQLException e){
      System.out.println("La table demandée n'existe pas.");
    }
    return true;
  }
}
