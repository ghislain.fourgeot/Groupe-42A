
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class ControleurBoutonConnexion implements ActionListener{
	LabelInterfaceConnexion vue;
	ControleurBoutonConnexion(LabelInterfaceConnexion vue){
		this.vue = vue;
	}
	public void actionPerformed(ActionEvent arg0) {
		if(vue.vue.c.bonCompte(vue.nomUtil.getText(),vue.mdp.getText())){
			switch(vue.vue.c.getRole(vue.nomUtil.getText(),vue.mdp.getText())){
			case 1:
				try {
					vue.vue.afficherCreation();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			case 2:
				try {
					vue.vue.afficherVueAppel();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			case 3:
				break;
			}
		}
		else{
			JOptionPane.showConfirmDialog(((Component)arg0.getSource()).getParent(),"Le nom d'utilisateur, ou le mot de passe est incorrect","Attention !",JOptionPane.YES_OPTION);
		}
	}

}
