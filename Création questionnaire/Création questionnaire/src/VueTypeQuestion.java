
import java.awt.FlowLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

//Panel du choix du type de la question
public class VueTypeQuestion extends JPanel {
	
	//Attributs
	JComboBox typeReponse;
	VueCreaionQuest vue;
	
	//Constructeur
	VueTypeQuestion(VueCreaionQuest vue){
		
		//Variables
		this.vue = vue;
		
		this.setLayout(new FlowLayout());
		JLabel choixTypeQ = new JLabel("Choix type de question : ");
		Object[] valeurs = {"Choix multiples","Choix unique","Réponse courte","Classement","Notes"};
		this.typeReponse = new JComboBox(valeurs);
		typeReponse.addItemListener(new ControleurTypeQuestionChange(vue));
		this.add(choixTypeQ);
		this.add(typeReponse);
	}
}
