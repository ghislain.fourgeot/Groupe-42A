import java.awt.*;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.*;
@SuppressWarnings("serial")
public class VueQuestionnaire extends JPanel{
	VuePanelQuestions questions;
	ArrayList<Question> lesquestions;
	VueQuestionnaireFixe infos;

	ControleurQuestionnaire controleurQ;
	Coordonnees coordonnees;
	InterfaceConnexion principale;
	ModeleBD bd;

	//Coordonnees sondeCourant;
	int sondeCourant;
	public VueQuestionnaire(InterfaceConnexion principale) throws SQLException{
		super();
		this.principale = principale;
		this.bd = principale.bd;
		this.setLayout(new BorderLayout());

		// controleur qui gere la vue questionnaire
		controleurQ = new ControleurQuestionnaire(this);

		// jpanel de gauche contient les questions
		JPanel questionnaire = new JPanel();
		questionnaire.setLayout(new BoxLayout(questionnaire,BoxLayout.Y_AXIS));
		questionnaire.setBorder(BorderFactory.createTitledBorder("Questionnaire"));

		//panel boutons coté questionnaire
		VueBouton bValider= new VueBouton("Valider",Color.GREEN);
		bValider.setLayout(new FlowLayout(FlowLayout.RIGHT));
		bValider.setAlignmentX(0);
		bValider.b.addActionListener(controleurQ);

		//ajout des questions sur le panel questionnaire

		coordonnees = bd.getCoordonnees();
		String titreQ= coordonnees.getIntituleS();
		
		int idQ = bd.getIdQuestionnaire(titreQ);

		lesquestions = bd.listeDesQuestions(idQ);
		questions = new VuePanelQuestions(this,lesquestions,idQ);
		JScrollPane scroll = ajoutScroll(questions);

		questionnaire.add(scroll);
		questionnaire.add(bValider);

		// le panel qui gère toute la partie information de la vueQuestionnaire
		infos = new VueQuestionnaireFixe(this);

		// ajout du bouton annuler questionnaire au controleur
		infos.bAnnuler.b.addActionListener(controleurQ);

		this.add(infos,"East");
		this.add(questionnaire,"Center");
	}
	public JScrollPane ajoutScroll(VuePanelQuestions p){
		JScrollPane res = new JScrollPane(p);
		return res;

	}

}
