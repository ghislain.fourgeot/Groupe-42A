
import java.awt.BorderLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

//Panel du titre et de la description du questionnaire
public class VueInformationsCreation extends JPanel{
	
	//Attributs
	JTextField titreText;
	JComboBox panel;
	
	//Constructeur
	VueInformationsCreation(){
		super();
		
		//Variables
		this.titreText = new JTextField(15);
		
		JPanel info_desc1 = new JPanel();
		JPanel info_desc2 = new JPanel();
		JLabel choixTitre = new JLabel("Titre questionnaire : ");
		JLabel description = new JLabel("Panel du questionnaire : ");
		String[] panelString = {"France global 1","Moins de 50 ans","France global 2"};
		panel = new JComboBox(panelString);
		info_desc1.add(choixTitre);
		info_desc1.add(titreText);
		info_desc2.add(description);
		info_desc2.add(panel);
		this.setLayout(new BorderLayout());
		this.add(info_desc1,BorderLayout.NORTH);
		this.add(info_desc2);
	}
}
