
import javax.swing.JButton;
import javax.swing.JPanel;


public class VueBoutonPetitQuest extends JPanel {
	JButton exploi;
	JButton supp;
	JButton modif;
	VueBoutonPetitQuest(VueQuestionnairePetit vue){
		this.exploi = new JButton("Mettre en exploitation");
		exploi.addActionListener(new ControleurExploitation(vue));
		this.supp = new JButton("Supprimer");
		supp.addActionListener(new ControleurSuppQuestionnaireListe(vue));
		this.modif = new JButton("Modifier");
		modif.addActionListener(new ControleurModifier(vue));
		this.add(exploi);
		this.add(supp);
		this.add(modif);
	}
}
