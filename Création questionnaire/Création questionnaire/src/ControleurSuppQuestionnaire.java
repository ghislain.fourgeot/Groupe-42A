import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JOptionPane;

//Controleur du bouton "Supprimer question"
public class ControleurSuppQuestionnaire implements ActionListener{
	
	//Attributs
	VueCreationQuestionnaire vueCrea;
	VueSuppQuest vueQuestion;
	
	//Constructeur
	ControleurSuppQuestionnaire(VueCreationQuestionnaire vue){
		
		//Variables
		this.vueCrea = vue;
	}
	
	//Suppression d'une question lors de l'appui sur le bouton
	public void actionPerformed(ActionEvent arg0) {
		int reply = JOptionPane.showConfirmDialog(((Component)arg0.getSource()).getParent(),"Voulez-vous quitter ce questionnaire sans avoir sauvegardé les modifications ?","Attention !",JOptionPane.YES_NO_OPTION);
		if(reply == JOptionPane.YES_OPTION){
			try {
				vueCrea.crea.afficherVueListeQuestionnaire();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}