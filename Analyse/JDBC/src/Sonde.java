import java.sql.Date;

class Sonde{
	int numSond;
	String nomSond;
	String prenomSond;
	Date dateNaisSond;
	String telephoneSond;
	String idC;
	
	Sonde(int numSond,String nomSond,String prenomSond,
	Date dateNaisSond,String telephoneSond,String idC){
		this.numSond=numSond;
		this.nomSond=nomSond;
		this.prenomSond=prenomSond;
		this.dateNaisSond=dateNaisSond;
		this.telephoneSond=telephoneSond;
		this.idC=idC;
	}
}