import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.*;

class VueListeSondage extends JPanel{
	
	VueListeSondage(ArrayList<VuedonneeSondage> liste,VueAccueilAnalyse vue){
		this.setLayout(new GridLayout(liste.size(),1));
		for (int i=0;i<liste.size();i++){
			this.add(creation(liste.get(i),vue));
		}
	}
/*
	VueListeSondage(ArrayList<VuedonneeSondage> liste){
		this.setLayout(new GridLayout(liste.size(),1));
		for (int i=0;i<liste.size();i++){
			this.add(creation(liste.get(i)));

		}
	}
*/
	public JPanel creation(VuedonneeSondage vue,VueAccueilAnalyse vue2){
		JPanel res = new JPanel();
		res.setLayout(new BoxLayout(res,BoxLayout.LINE_AXIS));
		JButton c = new JButton("Consulter");
		c.setSize(20,10);
		c.addActionListener(new ControlleurConsulter(vue2));
		JPanel butt = new JPanel();
		butt.setLayout(new BorderLayout());
		butt.add(Box.createRigidArea(new Dimension(10,25)),"North");
		butt.add(Box.createRigidArea(new Dimension(10,25)),"South");
		butt.add(Box.createRigidArea(new Dimension(25,10)),"East");
		butt.add(Box.createRigidArea(new Dimension(25,10)),"West");
		butt.add(c,"Center");

		vue.add(butt,"East");
		res.add(vue, "North");
		res.add(new JLabel(" "),"South");
		res.setPreferredSize(new Dimension(200,75));
		return res;
	}
}
