import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.*;

class VueAffichageQuestion extends JPanel{
	
	VueAffichageQuestion(VuedonneeSondage cli){
		this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		this.modifcli(cli);
		this.add(cli);
		VueQuestion q1 = new VueQuestion("Quel est la couleur du cheval Blanc d'henri 4?","A",1,creationTableau(),new VueCommentaire());
		VueQuestion q2 = new VueQuestion("Qu'est-ce qui est petit et marron?","B",2,creationTableau(),new VueCommentaire());
		VueQuestion q3 = new VueQuestion("Elle est ou la poulette?","C",3,creationTableau(),new VueCommentaire());
		ArrayList<VueQuestion> liste = new ArrayList<VueQuestion>();
		liste.add(q1);
		liste.add(q2);
		liste.add(q3);
		this.add(AjoutScroll(new VueListeQuestion(liste)));
		
		
	}
	private JScrollPane AjoutScroll(JPanel vue){
		JScrollPane res = new JScrollPane(vue);
		res.setPreferredSize(new Dimension(100,225));
		return res;
	}
	private void modifcli(VuedonneeSondage cli){
		JPanel PDF =new JPanel();
		PDF.setLayout(new BorderLayout());
		PDF.add(Box.createRigidArea(new Dimension(10,25)),"North");
		PDF.add(Box.createRigidArea(new Dimension(10,25)),"South");
		PDF.add(Box.createRigidArea(new Dimension(25,10)),"East");
		PDF.add(Box.createRigidArea(new Dimension(25,10)),"West");
		JPanel boutons = new JPanel();
		JButton retour = new JButton("Retour");
		retour.addActionListener(new ControlleurConsulter(this));
		retour.setBackground(Color.red);
		JButton pdf = new JButton("Aperçu PDF");
		pdf.addActionListener(new ControlleurConsulter(this));
		boutons.add(retour);
		boutons.add(pdf);
		PDF.add(boutons,"Center");
		pdf.setBackground(Color.green);
		cli.add(PDF,"East");
	}
	private VueTableauResult creationTableau(){
		
		String[] entetes = {"Categorie", "Tranche", "sexe", "reponse majoritaire"};
		Object[][]donnees = new Object[][]{
                {"Johnathan", "Sykes",  true, "TENNIS"},
                {"Nicolas", "Van de Kampf", true, "FOOTBALL"},
                {"Damien", "Cuthbert",  true, "RIEN"},
                {"Corinne", "Valance", false, "FOOTBALL"},
                {"Emilie", "Schrödinger",  false,"FOOTBALL"},
                {"Delphine", "Duke",  false, "TENNIS"},
                {"Eric", "Trump",  true, "FOOTBALL"},
        };
		
		return new VueTableauResult(donnees,entetes);
		
	}
}