import java.awt.Container;
import java.util.ArrayList;

import javax.swing.*;

class VueTest extends JFrame{
	
	VueTest(){
		this.setSize (800 ,400);
		this.setResizable(false);
		Container cont = this.getContentPane();
		cont.add(new VueAccueilAnalyse());
		cont.add(new JLabel(" "),"North");
		cont.add(new JLabel(" "),"South");
		cont.add(new JLabel(" "),"East");
		cont.add(new JLabel(" "),"West");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void actualise(JPanel pan){
		this.getContentPane().removeAll();
		this.getContentPane().add(pan);
		this.getContentPane().validate();
		this.getContentPane().repaint();
	}
}