import java.awt.*;

import javax.swing.*;

class VueQuestion extends JPanel{
	VueTableauResult tableau;
	VueCommentaire comm;
	
	VueQuestion(String intituleQ, String typeQ,int num,VueTableauResult tab, VueCommentaire com){
		super();
		tableau=tab;
		comm=com;
		this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		this.add(new JSeparator());
		this.add(entete(intituleQ,typeQ,num));
		this.add(Box.createRigidArea(new Dimension(100,25)));
		this.add(tableau);
		this.add(Box.createRigidArea(new Dimension(100,25)));
		this.add(comm);
	}
	
	private JPanel entete (String intituleQ, String typeQ,int num){
		JPanel res = new JPanel();
		JPanel info = new JPanel();
		info.setLayout(new BorderLayout());
		info.add(new JLabel("Question"+num+" : "+intituleQ),"North");
		info.add(new JLabel("Type question :"+typeQ),"South");
		res.setLayout(new BoxLayout(res,BoxLayout.LINE_AXIS));
		res.add(info);
		res.add(Box.createRigidArea(new Dimension(100,50)));
		JButton Aperçu = new JButton("Aperçu question"+num);
		res.add(Aperçu);
		res.add(Box.createRigidArea(new Dimension(100,50)));
		res.setPreferredSize(new Dimension(200,75));
		return res;
	}

}