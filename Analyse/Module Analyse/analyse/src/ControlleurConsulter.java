import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

class ControlleurConsulter implements ActionListener{
	JPanel vue;
	ControlleurConsulter(JPanel vue){
		this.vue=vue;
	}
	public void actionPerformed(ActionEvent arg0) {
		VueTest host =(VueTest) vue.getRootPane().getParent();
		switch(((JButton) arg0.getSource()).getText())
		{
		case "Consulter" :
			host.actualise(new VueAffichageQuestion(new VuedonneeSondage("testIut","Bob&Co",9)));
			break;
		case "Retour" :
			host.actualise(new VueAccueilAnalyse());
			break;
		case "Aperçu PDF":
			VuedonneeSondage cli= new VuedonneeSondage("testIut","Bob&Co",9);
			ArrayList<VueAperçu> questions = new ArrayList<VueAperçu>();
			host.actualise(new VueAperçuPDF(cli,questions));
			break;
		}
	}
	
}