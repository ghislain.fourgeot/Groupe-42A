import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.*;

class VueAperçuPDF extends JPanel{
	VueAperçuPDF(VuedonneeSondage cli,ArrayList<VueAperçu> questions){
		JPanel bouttons = new JPanel();
		bouttons.setLayout(new BoxLayout(bouttons,BoxLayout.LINE_AXIS));
		bouttons.add(new JButton("Retour"));
		bouttons.add(new JButton("Envoyer"));
		this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		this.add(cli);
		JPanel Qs = new JPanel();
		Qs.setLayout(new GridLayout(questions.size(),1));
		for (int i=0;i<questions.size();i++){
			Qs.add((questions.get(i)));
		this.add(Qs);
		}
	}
}