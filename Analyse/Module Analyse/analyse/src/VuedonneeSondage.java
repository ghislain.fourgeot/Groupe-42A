import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.*;

class VuedonneeSondage extends JPanel{
	VuedonneeSondage(String intitule, String client, int resultat){
		super();
		this.setLayout(new BorderLayout());
		JPanel text = new JPanel();
		text.setLayout(new BorderLayout());
		text.add(this.creation("Intitulé du Sondage : ",intitule),"North");
		text.add(this.creation("Société cliente : ",client),"Center");
		text.add(this.creation("Nombre de réponses reccueilli : ",""+resultat),"South");
		this.add(text, "West");
		this.setBorder(BorderFactory.createLineBorder(Color.black));
		this.setPreferredSize(new Dimension(300, 75));
	}
	public JPanel creation(String gauche, String droite){
		JLabel est = new JLabel(gauche);
		JLabel ouest = new JLabel(droite);
		JPanel res = new JPanel();
		res.add(est,"West");
		res.add(ouest,"East");
		return res;
	}
}
