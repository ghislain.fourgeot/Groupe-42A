import java.sql.*;

class Sonde{
	public int numSond;
	public String nomSond;
	public String prenomSond;
	public Date dateNaisSond;
	public String telephoneSond;
	public String idC;

	public Sonde(int numSond,String nomSond,String prenomSond,
	Date dateNaisSond,String telephoneSond,String idC){
		this.numSond=numSond;
		this.nomSond=nomSond;
		this.prenomSond=prenomSond;
		this.dateNaisSond=dateNaisSond;
		this.telephoneSond=telephoneSond;
		this.idC=idC;
	}
}
