import java.sql.*;

class Questionnaire {
	public int idQ;
	public String Titre;
	public char Etat;
	public int numC;
	public int idU;
	public int idPan;

	public Questionnaire(int idQ,String Titre,char Etat,
	              int numC,int idU,int idPan){
		this.idQ=idQ;
		this.Titre=Titre;
		this.Etat=Etat;
		this.numC=numC;
		this.idU=idU;
		this.idPan=idPan;
	}
}
