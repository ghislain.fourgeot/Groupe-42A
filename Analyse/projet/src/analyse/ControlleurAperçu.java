package analyse;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.jfree.data.general.DefaultPieDataset;

class ControlleurAperçu implements ActionListener{
	VueQuestion vue;
	VueTest host;
	ControlleurAperçu(VueQuestion vue, VueTest host){
		this.vue=vue;
		this.host=host;
	}

	public void actionPerformed(ActionEvent arg0) {
		switch(((JButton) arg0.getSource()).getText())
		{
		case "Aperçu":
			switch(vue.comm.type.getSelectedIndex()){
			case 0:
				JOptionPane.showMessageDialog(null,"Aucun type de diagramme n'as été selectionner");
				break;
			//genere un diagramme circulaire
			case 1:
				try {
					host.actualise(new VueAperçu(vue.intituleQ,vue.num,host.r.creationAperçuQPie(vue.intituleQ, vue.num),vue));
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println(e.getMessage());
				}
				break;
				
			//genere un diagramme a barre verticale
			case 2:
				try {
					host.actualise(new VueAperçu(vue.intituleQ,vue.num,host.r.creationAperçuQBar(vue.intituleQ, vue.num,true),vue));
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println(e.getMessage());
				}
				break;
			
			//genere un diagramme a barre Horizontal
			case 3:
				try {
					host.actualise(new VueAperçu(vue.intituleQ,vue.num,host.r.creationAperçuQBar(vue.intituleQ, vue.num,false),vue));
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println(e.getMessage());
				}
				break;
			
			//genere une courbe
			case 4:
				try {
					host.actualise(new VueAperçu(vue.intituleQ,vue.num,host.r.creationAperçuQLine(vue.intituleQ, vue.num),vue));
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println(e.getMessage());
				}
				break;
				
			//genere un tableau
			case 5:
				try {
					host.actualise(new VueAperçu(vue.intituleQ,vue.num,host.r.creationAperçuQTableau(vue.intituleQ, vue.num),vue));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			}
			break;
		
		//permet de revenir de l'Aperçu a l'affichage des résultats
		case "Retour" :
			host.actualise((JPanel) SwingUtilities.getAncestorNamed("AffichageQuestion", vue));
			break;
			
			
		}
		
	}
	
}