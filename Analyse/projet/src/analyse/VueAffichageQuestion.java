package analyse;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.*;

class VueAffichageQuestion extends JPanel{
	
	RequeteAnalyse r;
	VueTest host ;
	VuedonneeSondage vue ;
	ArrayList<VueQuestion> liste;
	
	VueAffichageQuestion(VuedonneeSondage cli,VueTest host) throws SQLException{
		this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		this.host=host;
		this.add(cli);
		this.vue=cli;
		this.setName("AffichageQuestion");
		this.r=host.r;
		this.modifcli(host);
		liste = r.createAffichageQuestion(cli.intitule, cli.cli);
		this.add(AjoutScroll(new VueListeQuestion(liste)));
		
		
	}
	private JScrollPane AjoutScroll(JPanel vue){
		JScrollPane res = new JScrollPane(vue);
		res.setPreferredSize(new Dimension(400,480));
		return res;
	}
	
	// sert a modifer la VuedonneeSondage
	private void modifcli(VueTest host){
		JPanel PDF =new JPanel();
		PDF.setLayout(new BorderLayout());
		
		//pour cadrer les boutons 
		PDF.add(Box.createRigidArea(new Dimension(10,25)),"North");
		PDF.add(Box.createRigidArea(new Dimension(10,25)),"South");
		PDF.add(Box.createRigidArea(new Dimension(25,10)),"East");
		PDF.add(Box.createRigidArea(new Dimension(25,10)),"West");
		
		//bouton pour revenir 
		JPanel boutons = new JPanel();
		JButton retour = new JButton("Retour");
		retour.addActionListener(new ControlleurConsulter(vue,host));
		retour.setBackground(Color.red);
		
		//bouton pour générer l'aperçu du pdf 
		JButton pdf = new JButton("Aperçu PDF");
		pdf.addActionListener(new ControlleurConsulter(vue,host));
		boutons.add(retour);
		boutons.add(pdf);
		PDF.add(boutons,"Center");
		pdf.setBackground(Color.green);
		vue.add(PDF,"East");
	
	}
}