package analyse;

import java.sql.*;

public class ConnexionMySQL {
	Connection mysql=null;
	boolean connecte=false;
	public ConnexionMySQL(String nomServeur, String nomBase, String nomLogin, String motDePasse) throws SQLException {
		try{
			Class.forName("com.mysql.jdbc.Driver");
			mysql=DriverManager.getConnection("jdbc:mysql://"+nomServeur+":3306/"+nomBase,nomLogin,motDePasse);
			connecte = true;
		}
	  catch(SQLException ex){
			System.out.println("Msg: "+ex.getMessage()+ ex.getErrorCode());
		}
		catch(ClassNotFoundException ex){
			System.out.println("Driver non trouver");
			mysql=null;
			return;
		}
	}
}
