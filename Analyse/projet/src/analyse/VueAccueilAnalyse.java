package analyse;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.*;

class VueAccueilAnalyse extends JPanel{
	public RequeteAnalyse r;
	public VuedonneeSondage vue;
	
	VueAccueilAnalyse(VueTest host)throws SQLException{
		this.setLayout(new BorderLayout ());
		this.r = host.r;
		VueListeSondage liste = new VueListeSondage(r.createAccueilAnalyse(),host);
		this.add(AjoutScroll(liste));
		
	}
	
	private JScrollPane AjoutScroll(VueListeSondage vue){
		JScrollPane res = new JScrollPane(vue);
		res.setPreferredSize(new Dimension(100,225));
		return res;
		
	}
}