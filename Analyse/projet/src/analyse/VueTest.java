package analyse;

import java.awt.BorderLayout;
import java.awt.Container;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.*;

import org.jfree.data.general.DefaultPieDataset;


public class VueTest extends JFrame{
	ConnexionMySQL co;
	RequeteAnalyse r;
	JPanel pan;
	public VueTest() throws Exception{
		co = new ConnexionMySQL("192.168.82.168","dbfourgeot","fourgeot","Billy");
		r= new RequeteAnalyse(this.co,this);
		this.setSize (1000 ,800);
		this.setResizable(false);
		Container cont = this.getContentPane();
		cont.setLayout(new BorderLayout());
		cont.add(new VueAccueilAnalyse(this));
		cont.add(new JLabel(" "),"North");
		cont.add(new JLabel(" "),"South");
		cont.add(new JLabel(" "),"East");
		cont.add(new JLabel(" "),"West");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
	
	
	public void actualise(JPanel pan){
		this.getContentPane().removeAll();
		this.getContentPane().add(pan);
		this.getContentPane().validate();
		this.getContentPane().repaint();
	}
}